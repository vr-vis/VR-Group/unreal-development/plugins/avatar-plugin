# Avatar Plugin

Plugin to visualize a user avatar when using HMD-based VR

For a How To Use, take a look at the [Wiki](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/plugins/avatar-plugin/-/wikis/home).
