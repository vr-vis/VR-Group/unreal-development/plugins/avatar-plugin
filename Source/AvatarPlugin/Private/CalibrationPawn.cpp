// Fill out your copyright notice in the Description page of Project Settings.


#include "CalibrationPawn.h"
#include <EnhancedInputComponent.h>
#include <EnhancedInputSubsystems.h>

ACalibrationPawn::ACalibrationPawn(const FObjectInitializer& ObjectInitializer) : ARWTHVRPawn(ObjectInitializer) {

	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0; // Necessary for receiving motion controller events.

	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	//----init other vars----

	calibrationComponent = nullptr;

	static ConstructorHelpers::FObjectFinder<UHapticFeedbackEffect_Curve> forceFeedbackObject(TEXT("/AvatarPlugin/MotionControllerHaptics"));
	if (forceFeedbackObject.Succeeded()) {
		rumbleCurve = forceFeedbackObject.Object;
	}

	//----init components----

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	Mesh->SetupAttachment(RootComponent);
	Mesh->AddWorldRotation(FRotator(0, -90, 0));

	VROrigin = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
	VROrigin->SetupAttachment(RootComponent);
	VROrigin->SetRelativeLocation(FVector(0, 0, 0));

	Capsule = CreateDefaultSubobject<UCapsuleComponent>("Capsule");
	Capsule->SetupAttachment(RootComponent);
	Capsule->SetCapsuleHalfHeight(80);
	Capsule->SetRelativeLocation(FVector(0, 0, 80));

	MC_LeftFoot = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MC_LeftFoot"));
	MC_LeftFoot->SetupAttachment(VROrigin);
	MC_LeftFoot->SetTrackingMotionSource("Tracker_Foot_Left");

	MC_RightFoot = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MC_RightFoot"));
	MC_RightFoot->SetupAttachment(VROrigin);
	MC_RightFoot->SetTrackingMotionSource("Tracker_Foot_Right");

	Mesh_LeftHand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh_LeftHand"));
	Mesh_LeftHand->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> sphereMesh1(TEXT("/Engine/BasicShapes/Sphere"));
	if (sphereMesh1.Succeeded()) {
		Mesh_LeftHand->SetStaticMesh(sphereMesh1.Object);
	}
	Mesh_LeftHand->SetRelativeScale3D(FVector(0.0625f, 0.0625f, 0.0625f));

	Mesh_RightHand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh_RightHand"));
	Mesh_RightHand->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> sphereMesh2(TEXT("/Engine/BasicShapes/Sphere"));
	if (sphereMesh2.Succeeded()) {
		Mesh_RightHand->SetStaticMesh(sphereMesh2.Object);
	}
	Mesh_RightHand->SetRelativeScale3D(FVector(0.0625f, 0.0625f, 0.0625f));

	Mesh_LeftFoot = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh_LeftFoot"));
	Mesh_LeftFoot->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> sphereMesh3(TEXT("/Engine/BasicShapes/Sphere"));
	if (sphereMesh3.Succeeded()) {
		Mesh_LeftFoot->SetStaticMesh(sphereMesh3.Object);
	}
	Mesh_LeftFoot->SetRelativeScale3D(FVector(0.0625f, 0.0625f, 0.0625f));

	Mesh_RightFoot = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh_RightFoot"));
	Mesh_RightFoot->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> sphereMesh4(TEXT("/Engine/BasicShapes/Sphere"));
	if (sphereMesh4.Succeeded()) {
		Mesh_RightFoot->SetStaticMesh(sphereMesh4.Object);
	}
	Mesh_RightFoot->SetRelativeScale3D(FVector(0.0625f, 0.0625f, 0.0625f));

	//setup motion copntroller visualizations
	RightControllerVis = CreateDefaultSubobject<UXRDeviceVisualizationComponent>(TEXT("RightControllerVisualization"));
	RightControllerVis->SetupAttachment(RightHand);
	LeftControllerVis = CreateDefaultSubobject<UXRDeviceVisualizationComponent>(TEXT("LeftControllerVisualization"));
	LeftControllerVis->SetupAttachment(LeftHand);
	RightFootControllerVis = CreateDefaultSubobject<UXRDeviceVisualizationComponent>(TEXT("RightFootControllerVisualization"));
	RightFootControllerVis->SetupAttachment(MC_RightFoot);
	LeftFootControllerVis = CreateDefaultSubobject<UXRDeviceVisualizationComponent>(TEXT("LeftFootControllerVisualization"));
	LeftFootControllerVis->SetupAttachment(MC_LeftFoot);


}

void ACalibrationPawn::BeginPlay() {
	Super::BeginPlay();

	Mesh->SetVisibility(false, true);

	if (calibrationComponent != nullptr) {
		ShowMeshes(calibrationComponent->showMeshes);
		ShowControllerModels(calibrationComponent->showControllers);
	}

}

void ACalibrationPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UAvatarAnimInstance* AI = Cast<UAvatarAnimInstance>(Mesh->GetAnimInstance());
	if (AI && !AI->CalibrationComponent) {
		AI->CalibrationComponent = calibrationComponent;
	}
}

void ACalibrationPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Get the player controller
	APlayerController* PC = Cast<APlayerController>(GetController());

	// Get the local player subsystem
	UEnhancedInputLocalPlayerSubsystem* EIPS = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer());
	// Clear out existing mapping, and add our mapping
	EIPS->AddMappingContext(CalibrationInputMapping, 0);

	UEnhancedInputComponent* EIC = Cast<UEnhancedInputComponent>(PlayerInputComponent);

	EIC->BindAction(SubmitRight, ETriggerEvent::Started, this, &ACalibrationPawn::RightTriggerPressed);
	EIC->BindAction(SubmitRight, ETriggerEvent::Completed, this, &ACalibrationPawn::RightTriggerReleased);
	EIC->BindAction(SubmitLeft, ETriggerEvent::Started, this, &ACalibrationPawn::LeftTriggerPressed);
	EIC->BindAction(SubmitLeft, ETriggerEvent::Completed, this, &ACalibrationPawn::LeftTriggerReleased);

	EIC->BindAction(RestartRight, ETriggerEvent::Started, this, &ACalibrationPawn::RightRestartPressed);
	EIC->BindAction(RestartRight, ETriggerEvent::Completed, this, &ACalibrationPawn::RightRestartReleased);
	EIC->BindAction(RestartLeft, ETriggerEvent::Started, this, &ACalibrationPawn::LeftRestartPressed);
	EIC->BindAction(RestartLeft, ETriggerEvent::Completed, this, &ACalibrationPawn::LeftRestartReleased);

}

void ACalibrationPawn::ShowMeshes(bool show) {
	Mesh_LeftHand->SetVisibility(show);
	Mesh_RightHand->SetVisibility(show);
	if ((!calibrationComponent) || (calibrationComponent && calibrationComponent->useFootControllers)) {
		Mesh_LeftFoot->SetVisibility(show);
		Mesh_RightFoot->SetVisibility(show);
	}
	else {
		Mesh_LeftFoot->SetVisibility(false);
		Mesh_RightFoot->SetVisibility(false);
	}
}

void ACalibrationPawn::ShowControllerModels(bool show) {
	if (LeftHand && LeftControllerVis) {
		LeftControllerVis->SetIsVisualizationActive(show);
	}
	if (RightHand && RightControllerVis) {
		RightControllerVis->SetIsVisualizationActive(show);
	}
	if ((!calibrationComponent) || (calibrationComponent && calibrationComponent->useFootControllers)) {
		LeftFootControllerVis->SetIsVisualizationActive(show);
		RightFootControllerVis->SetIsVisualizationActive(show);
	}
	else {
		LeftFootControllerVis->SetIsVisualizationActive(false);
		RightFootControllerVis->SetIsVisualizationActive(false);
	}
}

void ACalibrationPawn::SetupSkeletalMesh(USkeletalMesh* mesh) {
	Mesh->SetSkeletalMesh(mesh);
	Mesh->SetAnimInstanceClass(calibrationComponent->AnimBPToUse);
	Mesh->SetVisibility(true, true);
}

void ACalibrationPawn::SetCalibrationComponent(UAvatarComponent* component) {
	calibrationComponent = component;
	ShowMeshes(calibrationComponent->showMeshes);
	ShowControllerModels(calibrationComponent->showControllers);
}

void ACalibrationPawn::UpdateVRPose(FVRPose& pose, FVRPose& pose_no_pos_offsets, bool useFootControllers, bool correctVive, bool HandsSwitched, bool FeetSwitched, bool relative) {

	if (!relative) {

		pose.HMDPos = HeadCameraComponent->GetComponentLocation();
		pose.HMDRot = HeadCameraComponent->GetComponentRotation();
		if (!HandsSwitched) {
			pose.LeftHandPos = LeftHand->GetComponentLocation();
			pose.LeftHandRot = LeftHand->GetComponentRotation();
			pose.RightHandPos = RightHand->GetComponentLocation();
			pose.RightHandRot = RightHand->GetComponentRotation();
		}
		else {
			pose.RightHandPos = LeftHand->GetComponentLocation();
			pose.RightHandRot = LeftHand->GetComponentRotation();
			pose.LeftHandPos = RightHand->GetComponentLocation();
			pose.LeftHandRot = RightHand->GetComponentRotation();
		}

		if (useFootControllers) {
			if (!FeetSwitched) {
				pose.LeftFootPos = MC_LeftFoot->GetComponentLocation();
				pose.LeftFootRot = MC_LeftFoot->GetComponentRotation();
				pose.RightFootPos = MC_RightFoot->GetComponentLocation();
				pose.RightFootRot = MC_RightFoot->GetComponentRotation();
			}
			else {
				pose.RightFootPos = MC_LeftFoot->GetComponentLocation();
				pose.RightFootRot = MC_LeftFoot->GetComponentRotation();
				pose.LeftFootPos = MC_RightFoot->GetComponentLocation();
				pose.LeftFootRot = MC_RightFoot->GetComponentRotation();
			}
		}

		pose_no_pos_offsets = pose;

		//correct pose according to vive controllers
		if (correctVive) {

			if (!HandsSwitched) {
				pose.LeftHandPos -= (19 - 2) * LeftHand->GetForwardVector();
				pose.RightHandPos -= (19 - 2) * RightHand->GetForwardVector();
			}
			else {
				pose.RightHandPos -= (19 - 2) * LeftHand->GetForwardVector();
				pose.LeftHandPos -= (19 - 2) * RightHand->GetForwardVector();
			}

		}

	}

	else {

		pose.HMDPos = HeadCameraComponent->GetRelativeLocation();
		pose.HMDRot = HeadCameraComponent->GetRelativeRotation();
		if (!HandsSwitched) {
			pose.LeftHandPos = LeftHand->GetRelativeLocation();
			pose.LeftHandRot = LeftHand->GetRelativeRotation();
			pose.RightHandPos = RightHand->GetRelativeLocation();
			pose.RightHandRot = RightHand->GetRelativeRotation();
		}
		else {
			pose.RightHandPos = LeftHand->GetRelativeLocation();
			pose.RightHandRot = LeftHand->GetRelativeRotation();
			pose.LeftHandPos = RightHand->GetRelativeLocation();
			pose.LeftHandRot = RightHand->GetRelativeRotation();
		}

		if (useFootControllers) {
			if (!FeetSwitched) {
				pose.LeftFootPos = MC_LeftFoot->GetRelativeLocation();
				pose.LeftFootRot = MC_LeftFoot->GetRelativeRotation();
				pose.RightFootPos = MC_RightFoot->GetRelativeLocation();
				pose.RightFootRot = MC_RightFoot->GetRelativeRotation();
			}
			else {
				pose.RightFootPos = MC_LeftFoot->GetRelativeLocation();
				pose.RightFootRot = MC_LeftFoot->GetRelativeRotation();
				pose.LeftFootPos = MC_RightFoot->GetRelativeLocation();
				pose.LeftFootRot = MC_RightFoot->GetRelativeRotation();
			}
		}

		pose_no_pos_offsets = pose;

	}

}

void ACalibrationPawn::UpdateMeshPositions(const FVRPose& pose, bool relative) {

	if (!relative) {
		Mesh_LeftFoot->SetWorldLocation(pose.LeftFootPos);
		Mesh_RightFoot->SetWorldLocation(pose.RightFootPos);
		Mesh_LeftHand->SetWorldLocation(pose.LeftHandPos);
		Mesh_RightHand->SetWorldLocation(pose.RightHandPos);
	}
	else {
		Mesh_LeftFoot->SetRelativeLocation(pose.LeftFootPos);
		Mesh_RightFoot->SetRelativeLocation(pose.RightFootPos);
		Mesh_LeftHand->SetRelativeLocation(pose.LeftHandPos);
		Mesh_RightHand->SetRelativeLocation(pose.RightHandPos);
	}

}

void ACalibrationPawn::RumbleController(EControllerHand which, bool stop) {
	APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (stop) {
		pc->StopHapticEffect(which);
	}
	else {
		pc->PlayHapticEffect(rumbleCurve, which, 1.0f, true);
	}

}

void ACalibrationPawn::RightTriggerPressed() {
	right_trigger_down = true;
}

void ACalibrationPawn::RightTriggerReleased() {
	right_trigger_down = false;
}

void ACalibrationPawn::LeftTriggerPressed() {
	left_trigger_down = true;
}

void ACalibrationPawn::LeftTriggerReleased() {
	left_trigger_down = false;
}

void ACalibrationPawn::RightRestartPressed() {
	right_restart_down = true;
}

void ACalibrationPawn::RightRestartReleased() {
	right_restart_down = false;
}

void ACalibrationPawn::LeftRestartPressed() {
	left_restart_down = true;
}

void ACalibrationPawn::LeftRestartReleased() {
	left_restart_down = false;
}

