// Fill out your copyright notice in the Description page of Project Settings.

#include "AvatarComponent.h"
#include "CalibrationPawn.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "AvatarAnimInstance.h"
#include "GameFramework/PlayerStart.h"

UAvatarComponent::UAvatarComponent() {
	PrimaryComponentTick.bCanEverTick = true;

	calibrationMode = CalibrationMode::AVATARGUIDE;
	calibrationLevelPos = FVector(0, 0, 0);

	useFootControllers = true;
	DoFootStepAlgorithm = true;
	useLastCalibrationData = false;
	viveMode = true;
	HandsSwitched = false;
	FeetSwitched = false;
	showMeshes = false;
	showControllers = false;

	waitForLoad = false;
	stoppedCalibration = false;
	justLoadedWidget = false;
	widgetLoadedCompletely = false;
	isInspecting = false;
	isCalibrating = false;
	finishedCalibration = false;
	streamingLevel = nullptr;
	oldPossessedPawn = nullptr;
	pawn = nullptr;
	helperMesh = nullptr;
	leftFootSphere = nullptr;
	rightFootSphere = nullptr;
	widget = nullptr;

	waitForRelease = false;
	leftTriggerPressed = false;
	rightTriggerPressed = false;
	shouldPressLeftTrigger = false;
	shouldPressRightTrigger = false;
	prevPressedSuccess = false;
	doIK = false;

	footControllerHeightOffset = 10.f;

	static ConstructorHelpers::FObjectFinder<USoundWave> soundCue1(TEXT("SoundWave'/AvatarPlugin/Sounds/measureBuildUp.measureBuildUp'"));
	if (soundCue1.Succeeded()) {
		soundWaves.Add(TEXT("measureBuildUp"), soundCue1.Object);
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> texture1(TEXT("/AvatarPlugin/Images/Img_OculusController"));
	if (texture1.Succeeded()) {
		images.Add(TEXT("Img_OculusController"), texture1.Object);
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> texture2(TEXT("/AvatarPlugin/Images/Img_ViveController"));
	if (texture2.Succeeded()) {
		images.Add(TEXT("Img_ViveController"), texture2.Object);
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> texture3(TEXT("/AvatarPlugin/Images/Img_OculusFloorCalibration"));
	if (texture3.Succeeded()) {
		images.Add(TEXT("Img_OculusFloorCalibration"), texture3.Object);
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> texture4(TEXT("/AvatarPlugin/Images/Img_ViveFloorCalibration"));
	if (texture4.Succeeded()) {
		images.Add(TEXT("Img_ViveFloorCalibration"), texture4.Object);
	}

	static ConstructorHelpers::FObjectFinder<UAnimationAsset> Anim1(TEXT("/AvatarPlugin/HelperAnims/Anim_Welcome"));
	if (Anim1.Succeeded()) {
		Anim_Welcome = Anim1.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimationAsset> Anim3(TEXT("/AvatarPlugin/HelperAnims/Anim_ElbowWrist"));
	if (Anim3.Succeeded()) {
		Anim_ElbowWrist = Anim3.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimationAsset> Anim4(TEXT("/AvatarPlugin/HelperAnims/Anim_ShoulderWrist"));
	if (Anim4.Succeeded()) {
		Anim_ShoulderWrist = Anim4.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimationAsset> Anim5(TEXT("/AvatarPlugin/HelperAnims/Anim_ShoulderShoulder"));
	if (Anim5.Succeeded()) {
		Anim_ShoulderShoulder = Anim5.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimationAsset> Anim6(TEXT("/AvatarPlugin/HelperAnims/Anim_Hip"));
	if (Anim6.Succeeded()) {
		Anim_Hip = Anim6.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimationAsset> Anim7(TEXT("/AvatarPlugin/HelperAnims/Anim_Knee"));
	if (Anim7.Succeeded()) {
		Anim_Knee = Anim7.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimationAsset> Anim8(TEXT("/AvatarPlugin/HelperAnims/Anim_Done"));
	if (Anim8.Succeeded()) {
		Anim_Done = Anim8.Object;
	}
	
}

void UAvatarComponent::BeginPlay() {
	Super::BeginPlay();

	measurements = NewObject<UMeasurements>(UMeasurements::StaticClass());

	helperCapturePath = FPaths::ProjectPluginsDir() + "avatar-plugin/Content/captures_helper/";

	FName hmdDeviceName = UHeadMountedDisplayFunctionLibrary::GetHMDDeviceName();
	if (hmdDeviceName.ToString().Contains(TEXT("Oculus"))) {
		viveMode = false;
	}
	else {
		viveMode = true;
	}

}

void UAvatarComponent::InitMeasurements() {

	if (measurements) {

		MeasurementsInited = true;

		measurements->BoneNames = BoneNames;

		bool withAvatar = calibrationMode == CalibrationMode::AVATARGUIDE;
		measurements->Init(JsonInstructionsFilePath);
		TArray<MeasurementName> measSteps = measurements->defaultMeasurementSteps;
		if (calibrationMode != CalibrationMode::SCALE) {
			measSteps.Empty();
			measSteps.Append({
				MeasurementName::START,
				MeasurementName::WELCOME,
				MeasurementName::MEASLOCINFO,
				MeasurementName::HEIGHT,
				MeasurementName::ELBOW_WRIST,
				MeasurementName::SHOULDER_WRIST,
				MeasurementName::SHOULDER_SHOULDER,
				MeasurementName::HIP,
				MeasurementName::KNEE,
				MeasurementName::OFFSETS,
				MeasurementName::DONE
			});
		}
		measurements->ConfigureNewInstructions(JsonInstructionsFilePath, measSteps, withAvatar, useFootControllers, !viveMode, calibrationMode == CalibrationMode::SCALE);
		shouldPressLeftTrigger = measurements->measurements[0].leftShouldBePressed;
		shouldPressRightTrigger = measurements->measurements[0].rightShouldBePressed;

		if (useLastCalibrationData) {
			bool loaded = measurements->LoadFromFileAndConfigureInstructions(CalibrationDataSaveFile, JsonInstructionsFilePath, withAvatar, useFootControllers, !viveMode, calibrationMode == CalibrationMode::SCALE, HandsSwitched, FeetSwitched);
			if (loaded) {
				finishedCalibration = true;
				if (HandsSwitched) {
					SwitchHands();
				}
				if (useFootControllers && FeetSwitched) {
					SwitchFeet();
				}
			}
			SkeletalMeshComponent->SetAnimInstanceClass(AnimBPToUse);
			doIK = true;
		}

	}
	else {
		measurements = NewObject<UMeasurements>(UMeasurements::StaticClass());
	}
}

void UAvatarComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!MeasurementsInited) {
		InitMeasurements();
	}
	if (!MeasurementsInited) {
		return;
	}

	UAvatarAnimInstance* AI = Cast<UAvatarAnimInstance>(SkeletalMeshComponent->GetAnimInstance());
	if (AI && !AI->CalibrationComponent) {
		AI->CalibrationComponent = this;
	}

	if (isCalibrating) {

		if (waitForLoad) {
			if (streamingLevel->IsLevelLoaded()) {
				waitForLoad = false;
				OnLevelLoaded();
			}
		}
		else if (!widgetLoadedCompletely) {
			TryLoadWidget();
		}
		else {

			APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

			if (pawn) {
				leftTriggerPressed = pawn->left_trigger_down;
				rightTriggerPressed = pawn->right_trigger_down;
			}
			else {
				leftTriggerPressed = false;
				rightTriggerPressed = false;
			}

			//if (!isInspecting) {
			UpdateCalibration();
			//}
			//else {
			//	UpdateInspection();
			//}

		}

		if (!isCalibrating) {
			OnStopCalibration();
		}
	}

}

void UAvatarComponent::StartCalibration() {

	stoppedCalibration = false;
	doIK = false;
	useLastCalibrationData = false;
	measurements->lockCalcAppliedLengths = true;
	measurements->calcedAppliedLengths = false;

	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	oldPossessedPawn = playerController->GetPawn();

	bool usePredefinedReal = false;
	bool usePredefinedShortArms = false;
	if (usePredefinedReal) {
		
		for (auto& measurement : measurements->measurements) {
			switch (measurement.enumName) {
			case MeasurementName::HEIGHT:
				measurement.measuredLength = 152.40f;
				break;
			case MeasurementName::ELBOW_WRIST:
				measurement.measuredLength = 30.05f;
				break;
			case MeasurementName::SHOULDER_WRIST:
				measurement.measuredLength = 53.95f;
				break;
			case MeasurementName::SHOULDER_SHOULDER:
				measurement.measuredLength = 33.87f;
				break;
			case MeasurementName::HIP:
				measurement.measuredLength = 25.61f;
				measurement.measuredAdditionalHeight = 80.53f;
				break;
			case MeasurementName::KNEE:
				measurement.measuredLength = 67.58f;
				break;
			default:
				break;
			}
		}

		finishedCalibration = true;
		return;

	}
	if (usePredefinedShortArms) {

		for (auto& measurement : measurements->measurements) {
			switch (measurement.enumName) {
			case MeasurementName::HEIGHT:
				measurement.measuredLength = 160.21f;
				break;
			case MeasurementName::ELBOW_WRIST:
				measurement.measuredLength = 31.65f;
				break;
			case MeasurementName::SHOULDER_WRIST:
				measurement.measuredLength = 41.65f;
				break;
			case MeasurementName::SHOULDER_SHOULDER:
				measurement.measuredLength = 36.18f;
				break;
			case MeasurementName::HIP:
				measurement.measuredLength = 26.22f;
				measurement.measuredAdditionalHeight = 94.5f;
				break;
			case MeasurementName::KNEE:
				measurement.measuredLength = 47.20f;
				break;
			default:
				break;
			}
		}

		finishedCalibration = true;
		return;

	}

	if (!isCalibrating) {

		measurements->ResetMeasurements();
		isCalibrating = true;

		bool success;
		streamingLevel = ULevelStreamingDynamic::LoadLevelInstance(GetWorld(), calibrationLevelPath, calibrationLevelPos, FRotator::ZeroRotator, success);
		
		if (!success) {
			isCalibrating = false;
			streamingLevel = nullptr;
		}

		waitForLoad = true;

	}
}

void UAvatarComponent::StopCalibration() {
	isCalibrating = false;
	OnStopCalibration();
}

bool UAvatarComponent::IsCalibrating() {
	return isCalibrating;
}

bool UAvatarComponent::IsInspecting() {
	return isInspecting;
}

bool UAvatarComponent::FinishedCalibration() {
	return !isCalibrating && finishedCalibration;
}

void UAvatarComponent::FinishCalibration() {
	StopCalibration();
	finishedCalibration = true;
}

float UAvatarComponent::GetConfirmTimePercentage() {
	return (pawn->GetGameTimeSinceCreation() - triggerPressStartTime) / confirmTime;
}

UMeasurements* UAvatarComponent::GetMeasurements() {
	return measurements;
}

bool UAvatarComponent::CalcAppliedLengths() {
	if (!IsCalibrating() && calibrationMode != CalibrationMode::SCALE) {
		measurements->CalcAppliedLengths(SkeletalMeshComponent);
		return true;
	}
	return false;
}

void UAvatarComponent::SetBoneNames(const FBoneNames& boneNames_) {
	BoneNames = boneNames_;
	measurements->BoneNames = BoneNames;
}

bool UAvatarComponent::CanEditChange(const FProperty* InProperty) const {
	if (InProperty->GetFName() == "DoFootStepAlgorithm" && useFootControllers) {
		return false;
	}
	return true;
}

void UAvatarComponent::OnStopCalibration() {
	if (!stoppedCalibration) {

		measurements->SaveToFile(CalibrationDataSaveFile, HandsSwitched, FeetSwitched);

		if (oldPossessedPawn != nullptr) {

			SkeletalMeshComponent = OldSkeletalMeshComponent;
			HeadComponent = OldHeadComponent;
			HandLeftComponent = OldHandLeftComponent;
			HandRightComponent = OldHandRightComponent;
			FootLeftComponent = OldFootLeftComponent;
			FootRightComponent = OldFootRightComponent;

			APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			playerController->UnPossess();
			playerController->Possess(oldPossessedPawn);
			SkeletalMeshComponent->SetAnimInstanceClass(AnimBPToUse);
		}

		if (streamingLevel != nullptr) {
			for (AActor* actor : streamingLevel->GetLoadedLevel()->Actors) {
				actor->Destroy();
			}
		}

		FLatentActionInfo LatentInfo;
		UGameplayStatics::UnloadStreamLevel(GetWorld(), *calibrationLevelPath, LatentInfo, true);
		streamingLevel = nullptr;

		stoppedCalibration = true;
		finishedCalibration = true;

	}
}

void UAvatarComponent::OnLevelLoaded() {
	
	for (AActor* actor : streamingLevel->GetLoadedLevel()->Actors) {
		
		if (Cast<ACalibrationPawn>(actor)) {
			pawn = Cast<ACalibrationPawn>(actor);
		}

		else if (Cast<ASkeletalMeshActor>(actor)) {
			helperMesh = Cast<ASkeletalMeshActor>(actor);
		}

		else if (actor->GetName().Contains("LeftFootSphere", ESearchCase::CaseSensitive)) {
			leftFootSphere = actor;
		}

		else if (actor->GetName().Contains("RightFootSphere", ESearchCase::CaseSensitive)) {
			rightFootSphere = actor;
		}

		else if (Cast<APlayerStart>(actor)) {
			mirrorPlayerStart = actor;
		}

	}

	if (pawn == nullptr) {
		for (AActor* actor : streamingLevel->GetLoadedLevel()->Actors) {
			actor->Destroy();
		}
		FLatentActionInfo LatentInfo;
		UGameplayStatics::UnloadStreamLevel(GetWorld(), *calibrationLevelPath, LatentInfo, true);
		streamingLevel = nullptr;
		isCalibrating = false;
		return;
	}

	pawn->SetCalibrationComponent(this);
	pawn->SetupSkeletalMesh(SkeletalMeshComponent->GetSkeletalMeshAsset());

	SkeletalMeshComponent->SetAnimInstanceClass(nullptr);
	
	OldSkeletalMeshComponent = SkeletalMeshComponent;
	OldHeadComponent = HeadComponent;
	OldHandLeftComponent = HandLeftComponent;
	OldHandRightComponent = HandRightComponent;
	OldFootLeftComponent = FootLeftComponent;
	OldFootRightComponent = FootRightComponent;

	SkeletalMeshComponent = pawn->Mesh;
	HeadComponent = pawn->HeadCameraComponent;
	HandLeftComponent = pawn->LeftHand;
	HandRightComponent = pawn->RightHand;
	FootLeftComponent = pawn->MC_LeftFoot;
	FootRightComponent = pawn->MC_RightFoot;

	if (helperMesh != nullptr) {
		if (calibrationMode == CalibrationMode::AVATARGUIDE) {
			//helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_Welcome, false);
		}
		else {
			helperMesh->SetActorHiddenInGame(true);
		}
	}

	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	oldPossessedPawn = playerController->GetPawn();
	playerController->UnPossess();
	playerController->Possess(pawn);

	if (!useFootControllers) {
		pawn->Mesh_LeftFoot->SetVisibility(false);
		pawn->Mesh_RightFoot->SetVisibility(false);
	}

}

void UAvatarComponent::StartInspection() {
	isInspecting = true;
	doIK = true;
}

void UAvatarComponent::UpdateCalibration() {

	pawn->UpdateVRPose(pose, pose_no_pos_offsets, useFootControllers, viveMode, HandsSwitched, FeetSwitched);
	
	if (!useFootControllers && leftFootSphere && rightFootSphere) {
		pose.LeftFootPos = leftFootSphere->GetActorLocation();
		pose.LeftFootPos.Z += footControllerHeightOffset; //simulate foot input tracker height level
		pose.LeftFootRot = leftFootSphere->GetActorRotation();
		pose.RightFootPos = rightFootSphere->GetActorLocation();
		pose.RightFootPos.Z += footControllerHeightOffset; //simulate foot input tracker height level
		pose.RightFootRot = rightFootSphere->GetActorRotation();
	}

	pawn->UpdateMeshPositions(pose);

	if (waitForRelease
		&& ((shouldPressLeftTrigger && shouldPressRightTrigger && (!leftTriggerPressed || !rightTriggerPressed))
			|| (shouldPressLeftTrigger && !leftTriggerPressed)
			|| (shouldPressRightTrigger && !rightTriggerPressed))) {

		waitForRelease = false;
	}

	if (shouldPressLeftTrigger || shouldPressRightTrigger) {

		if (!waitForRelease
			&& (!shouldPressLeftTrigger || leftTriggerPressed)
			&& (!shouldPressRightTrigger || rightTriggerPressed)) {

			if (!prevPressedSuccess) {
				PlaySound(TEXT("measureBuildUp"));

				if (shouldPressRightTrigger) {
					pawn->RumbleController(EControllerHand::Right, false);
				}
				if(shouldPressLeftTrigger) {
					pawn->RumbleController(EControllerHand::Left, false);
				}
				
				
				prevPressedSuccess = true;
			}

			if (pawn->GetGameTimeSinceCreation() - triggerPressStartTime >= confirmTime) {
				measurements->TakeMeasurement(pose, pose_no_pos_offsets, pawn->Mesh, pawn->GetActorLocation().Z);

				//calibrate scale and which trackers are left/right
				if (measurements->measurements[measurements->activeMeasurement].enumName == MeasurementName::HEIGHT) {
					
					measurements->appliedScale = pose.HMDPos.Z - pawn->GetActorLocation().Z;
					measurements->appliedScale += 7;
					measurements->appliedScale = measurements->appliedScale / MeshDefaultHeight;
					if (calibrationMode == CalibrationMode::SCALE) {
						pawn->Mesh->SetVisibility(true);
					}

					//left/right

					//a * HMDForwardDir + b * (0, 0, 1);
					//(P - HMDPose) * n = 0
					FVector HMDForwardDir = -pose.HMDRot.Quaternion().GetForwardVector();
					FVector Normal = FVector::CrossProduct(HMDForwardDir, FVector(0., 0., 1.));

					if (0. <= FVector::DotProduct(pose.LeftHandPos - pose.HMDPos, Normal)) {
						if (!HandsSwitched) {
							HandsSwitched = true;
							SwitchHands();
						}
					}
					else if (HandsSwitched) {
						HandsSwitched = false;
						SwitchHands();
					}

					if (useFootControllers && 0. <= FVector::DotProduct(pose.LeftFootPos - pose.HMDPos, Normal)) {
						if (!FeetSwitched) {
							FeetSwitched = true;
							SwitchFeet();
						}
					}
					else if (useFootControllers && FeetSwitched) {
						FeetSwitched = false;
						SwitchFeet();
					}

				}

				NextMeasurementStep();
				widget->progbar->SetPercent(0);
				pawn->RumbleController(EControllerHand::Right, true);
				pawn->RumbleController(EControllerHand::Left, true);
			}
			else {
				widget->progbar->SetPercent(GetConfirmTimePercentage());
			}

		}
		else {

			if (prevPressedSuccess) {
				StopSound(TEXT("measureBuildUp"));
				prevPressedSuccess = false;
				widget->progbar->SetPercent(0);
				pawn->RumbleController(EControllerHand::Right, true);
				pawn->RumbleController(EControllerHand::Left, true);
			}

			triggerPressStartTime = pawn->GetGameTimeSinceCreation();

		}

	}

	if (isInspecting) {
		if (pawn->left_restart_down || pawn->right_restart_down) {
			measurements->lockCalcAppliedLengths = true;
			measurements->ResetMeasurements();
			pawn->Mesh->SetVisibility(false);
			isInspecting = false;
			doIK = false;
			measurements->activeMeasurement = 1;
			SetWidgetInformation(measurements->activeMeasurement);
			shouldPressLeftTrigger = measurements->measurements[measurements->activeMeasurement].leftShouldBePressed;
			shouldPressRightTrigger = measurements->measurements[measurements->activeMeasurement].rightShouldBePressed;
		}
	}

}

void UAvatarComponent::UpdateInspection() {
	
	if (waitForRelease) {

		if (!leftTriggerPressed && !rightTriggerPressed) {
			waitForRelease = false;
		}

	}
	else if (leftTriggerPressed && rightTriggerPressed) {

		if (!prevPressedSuccess) {
			PlaySound(TEXT("measureBuildUp"));

			pawn->RumbleController(EControllerHand::Right, false);
			pawn->RumbleController(EControllerHand::Left, false);

			prevPressedSuccess = true;
		}

		if (pawn->GetGameTimeSinceCreation() - triggerPressStartTime >= confirmTime) {
			isInspecting = false;
			StopCalibration();
			pawn->RumbleController(EControllerHand::Right, true);
			pawn->RumbleController(EControllerHand::Left, true);
		}

	}
	else {

		if (prevPressedSuccess) {
			StopSound(TEXT("measureBuildUp"));
			prevPressedSuccess = false;
			pawn->RumbleController(EControllerHand::Right, true);
			pawn->RumbleController(EControllerHand::Left, true);
		}

		triggerPressStartTime = pawn->GetGameTimeSinceCreation();

	}
}

void UAvatarComponent::NextMeasurementStep() {

	int activeMeasurement = measurements->NextMeasurementStep();
	waitForRelease = true;

	if (activeMeasurement < measurements->Num() && measurements->measurements[activeMeasurement].enumName == MeasurementName::OFFSETS) {
		pawn->Mesh->SetVisibility(true);
		measurements->lockCalcAppliedLengths = false;
		measurements->calcedAppliedLengths = false;
	}

	if (activeMeasurement < measurements->Num() && measurements->measurements[activeMeasurement].enumName == MeasurementName::DONE) {
		StartInspection();
	}

	if (activeMeasurement >= measurements->Num()) {
		doIK = true;
		StopCalibration();
	}
	else {

		SetWidgetInformation(activeMeasurement);

		shouldPressLeftTrigger = measurements->measurements[activeMeasurement].leftShouldBePressed;
		shouldPressRightTrigger = measurements->measurements[activeMeasurement].rightShouldBePressed;

		if (calibrationMode == CalibrationMode::AVATARGUIDE && helperMesh != nullptr) {
			switch (measurements->measurements[activeMeasurement].enumName) {
			case MeasurementName::WELCOME:
				helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_Welcome, false);
				break;
			case MeasurementName::HEIGHT:
				break;
			case MeasurementName::ELBOW_WRIST:
				helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_ElbowWrist, false);
				break;
			case MeasurementName::SHOULDER_WRIST:
				helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_ShoulderWrist, false);
				break;
			case MeasurementName::SHOULDER_SHOULDER:
				helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_ShoulderShoulder, false);
				break;
			case MeasurementName::HIP:
				helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_Hip, false);
				break;
			case MeasurementName::KNEE:
				helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_Knee, false);
				break;
			case MeasurementName::OFFSETS:
				helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_Done, false);
				break;
			case MeasurementName::DONE:
				//helperMesh->GetSkeletalMeshComponent()->PlayAnimation(Anim_Done, false);
				break;
			default:
				break;
			}
		}

	}

}

void UAvatarComponent::TryLoadWidget() {

	if (widget == nullptr) {
		for (TActorIterator<AActor> Actor(GetWorld()); Actor; ++Actor)
		{
			UWidgetComponent* wComp = Cast<UWidgetComponent>(Actor->GetRootComponent());
			if (wComp) {
				UUserWidget* uWidg = wComp->GetUserWidgetObject();
				if (uWidg == nullptr) {
					wComp->InitWidget();
					uWidg = wComp->GetUserWidgetObject();
				}
				if (Cast<UCalibrationWidget>(uWidg)) {
					widget = Cast<UCalibrationWidget>(uWidg);
					justLoadedWidget = true;
				}
			}
		}
	}

	if (justLoadedWidget && widget->text_name != nullptr) {
		SetWidgetInformation(0);
		justLoadedWidget = false;
		widgetLoadedCompletely = true;
	}

}

void UAvatarComponent::SetWidgetInformation(int measurementNr) {

	widget->text_name->SetText(FText::FromName(measurements->measurements[measurementNr].name));
	widget->text_instructions->SetText(FText::FromString(measurements->measurements[measurementNr].instructions));
	if (!measurements->measurements[measurementNr].leftShouldBePressed) {
		FString pressRight = measurements->LoadTextsFromJson(JsonInstructionsFilePath) ? measurements->TextsJson->GetStringField("pressRight_instr") : "Hold right trigger...";
		widget->text_continue->SetText(FText::FromString(pressRight));
	}
	else if (!measurements->measurements[measurementNr].rightShouldBePressed) {
		FString pressLeft = measurements->LoadTextsFromJson(JsonInstructionsFilePath) ? measurements->TextsJson->GetStringField("pressLeft_instr") : "Hold left trigger...";
		widget->text_continue->SetText(FText::FromString(pressLeft));
	}
	else {
		FString pressBoth = measurements->LoadTextsFromJson(JsonInstructionsFilePath) ? measurements->TextsJson->GetStringField("pressBoth_instr") : "Hold left and right trigger...";
		widget->text_continue->SetText(FText::FromString(pressBoth));
	}

	if (measurements->measurements[measurementNr].imgPath.Compare("") == 0) {
		widget->help_image->SetBrushFromTexture(nullptr);
		widget->help_image->SetColorAndOpacity(FLinearColor(1, 1, 1, 0));
	}
	else {

		UTexture2D* texture = *images.Find(measurements->measurements[measurementNr].imgPath);

		if (texture != nullptr) {
			widget->help_image->SetBrushFromTexture(texture, false);
			widget->help_image->SetColorAndOpacity(FLinearColor(1, 1, 1, 0.7f));
		}
	}

}

void UAvatarComponent::PlaySound(const FName& name, float vol, bool threeDimensional, const FVector& loc) {
	if (soundWaves.Contains(name)) {
		if (playingSounds.Contains(name)) {
			StopSound(name);
		}
		if (!threeDimensional) {
			playingSounds.Add(name, UGameplayStatics::SpawnSound2D(GetWorld(), soundWaves[name], vol));
		}
		else {
			playingSounds.Add(name, UGameplayStatics::SpawnSoundAtLocation(GetWorld(), soundWaves[name], loc, FRotator::ZeroRotator, vol));
		}
	}
}

void UAvatarComponent::StopSound(const FName& name) {
	if (playingSounds.Contains(name)) {
		if (playingSounds[name] != nullptr && playingSounds[name]->IsActive()) {
			playingSounds[name]->SetActive(false);
		}
		playingSounds.Remove(name);
	}
}

void UAvatarComponent::SwitchHands() {
	USceneComponent* Tmp = HandRightComponent;
	HandRightComponent = HandLeftComponent;
	HandLeftComponent = Tmp;
	Tmp = OldHandRightComponent;
	OldHandRightComponent = OldHandLeftComponent;
	OldHandLeftComponent = Tmp;
}

void UAvatarComponent::SwitchFeet() {
	USceneComponent* Tmp = FootRightComponent;
	FootRightComponent = FootLeftComponent;
	FootLeftComponent = Tmp;
	Tmp = OldFootRightComponent;
	OldFootRightComponent = OldFootLeftComponent;
	OldFootLeftComponent = Tmp;
}
