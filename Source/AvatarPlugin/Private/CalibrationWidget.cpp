// Fill out your copyright notice in the Description page of Project Settings.


#include "CalibrationWidget.h"
#include "AvatarComponent.h"

UCalibrationWidget::UCalibrationWidget(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{}

void UCalibrationWidget::NativeConstruct() {
	Super::NativeConstruct();

	if (text_name != nullptr && text_instructions != nullptr && text_continue != nullptr) {
		text_name->SetText(FText::FromString(TEXT("")));
		text_instructions->SetText(FText::FromString(TEXT("")));
		text_continue->SetText(FText::FromString(TEXT("")));
	}
}

void UCalibrationWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime) {
	Super::NativeTick(MyGeometry, InDeltaTime);
}