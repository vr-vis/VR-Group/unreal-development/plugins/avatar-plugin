// Fill out your copyright notice in the Description page of Project Settings.


#include "CalibrationNode.h"

#include "Engine/GameEngine.h"

bool FCalibrationNode::SetUpSkeletonBones() {

    if (BonesToModify.Num() == 0) {

        const FBoneNames& names = measurements->BoneNames;

        BonesToModify.Empty();

        BonesToModify.Add(FBoneReference(names.boneroot));
        BonesToModify.Add(FBoneReference(names.spine01));
        BonesToModify.Add(FBoneReference(names.spine02));
        BonesToModify.Add(FBoneReference(names.spine03));
        BonesToModify.Add(FBoneReference(names.spine04));
        BonesToModify.Add(FBoneReference(names.spine05));

        BonesToModify.Add(FBoneReference(names.neck));
        BonesToModify.Add(FBoneReference(names.neck02));
        BonesToModify.Add(FBoneReference(names.head));

        BonesToModify.Add(FBoneReference(names.clavicle_l));
        BonesToModify.Add(FBoneReference(names.clavicle_r));
        BonesToModify.Add(FBoneReference(names.upperarm_l));
        BonesToModify.Add(FBoneReference(names.upperarm_r));
        BonesToModify.Add(FBoneReference(names.lowerarm_l));
        BonesToModify.Add(FBoneReference(names.lowerarm_r));
        BonesToModify.Add(FBoneReference(names.hand_l));
        BonesToModify.Add(FBoneReference(names.hand_r));
       
        BonesToModify.Add(FBoneReference(names.pelvis));
        BonesToModify.Add(FBoneReference(names.calf_l));
        BonesToModify.Add(FBoneReference(names.calf_r));
        BonesToModify.Add(FBoneReference(names.thigh_l));
        BonesToModify.Add(FBoneReference(names.thigh_r));
        BonesToModify.Add(FBoneReference(names.foot_l));
        BonesToModify.Add(FBoneReference(names.foot_r));
        BonesToModify.Add(FBoneReference(names.foot_socket_l));
        BonesToModify.Add(FBoneReference(names.foot_socket_r));

        return true;

    }

    return false;
}

void FCalibrationNode::InitializeBoneReferences(const FBoneContainer& RequiredBones) {
    DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(InitializeBoneReferences)
    for (int i = 0; i < BonesToModify.Num(); i++) {
        BonesToModify[i].Initialize(RequiredBones);
        if (!BonesToModify[i].HasValidSetup()) {
            BonesToModify.RemoveAt(i);
            i--;
        }
    }
}

FCalibrationNode::FCalibrationNode() {
    skeletalMesh = nullptr;
    measurements = nullptr;
}

struct FLengthMeas {
    float applied;
    FName endBone;
    FCompactPoseBoneIndex index;

    FLengthMeas(float applied, FName& endBone, FCompactPoseBoneIndex& index)
        : applied(applied), endBone(endBone), index(index)
    {};
};

struct FCompareBoneTransformIndexMeasurements {
    FORCEINLINE bool operator()(const FLengthMeas& A, const FLengthMeas& B) const
    {
        return A.index < B.index;
    }
};

void FCalibrationNode::EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) {
    DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(EvaluateSkeletalControl_AnyThread)
    check(OutBoneTransforms.Num() == 0);

    if (measurements == nullptr || skeletalMesh == nullptr || Alpha == 0.f) {
        return;
    }

    if (!measurements->calcedAppliedLengths) {
        measurements->CalcAppliedLengths(skeletalMesh);
        return;
    }

    const FBoneContainer& BoneContainer = Output.Pose.GetPose().GetBoneContainer();
    TMap<int32, FVector> transformed;

    bool bonesChanged = SetUpSkeletonBones();
    if (bonesChanged) {
        InitializeBoneReferences(BoneContainer);
    }

    float Scale = measurements->appliedScale;
    //float Scale = 1.f;

    TArray<FLengthMeas> bonesSorted;
    for (auto& measurement : measurements->appliedLengths) {

        FName bName = measurement.endBone;

        int boneIndex = -1;
        for (int j = 0; j < BonesToModify.Num(); j++) {
            if (bName.Compare(BonesToModify[j].BoneName) == 0) {
                boneIndex = j;
                break;
            }
        }
        if (boneIndex == -1) return;
        FCompactPoseBoneIndex boneToModify = BonesToModify[boneIndex].GetCompactPoseIndex(BoneContainer);

        bonesSorted.Add(FLengthMeas(measurement.applied, measurement.endBone, boneToModify));
    }

    bonesSorted.Sort(FCompareBoneTransformIndexMeasurements());

    for (int i = 0; i < bonesSorted.Num(); i++) {

        FLengthMeas& measurement = bonesSorted[i];

        //---get name and index of current bone---
        FName bName = measurement.endBone;

        FCompactPoseBoneIndex boneToModify = measurement.index;

        //---check that ancestor bones have been updated---
        {
            auto ParentIndex = Output.Pose.GetPose().GetParentBoneIndex(boneToModify);
            FVector* Found = transformed.Find(ParentIndex.GetInt());
            TArray<FCompactPoseBoneIndex> ToUpdate;
            while (Found == nullptr && !ParentIndex.IsRootBone()) {
                ToUpdate.Add(ParentIndex);
                ParentIndex = Output.Pose.GetPose().GetParentBoneIndex(ParentIndex);
                Found = transformed.Find(ParentIndex.GetInt());
            }
            if (!ParentIndex.IsRootBone()) {
                for (int j = ToUpdate.Num() - 1; j >= 0; j--) {
                    ParentIndex = Output.Pose.GetPose().GetParentBoneIndex(ToUpdate[j]);
                    FVector* ParentPos = transformed.Find(ParentIndex.GetInt());
                    FVector OldParentPos = Output.Pose.GetComponentSpaceTransform(ParentIndex).GetLocation();
                    FVector CurrentPos = Output.Pose.GetComponentSpaceTransform(ToUpdate[j]).GetLocation();
                    FVector ParentToCurrent = CurrentPos - OldParentPos;
                    FVector NewCurrentPos = *ParentPos + ParentToCurrent;
                    transformed.Add(ToUpdate[j].GetInt(), NewCurrentPos);
                }
            }
        }

        //---set bone length---
        FTransform BoneTrans = Output.Pose.GetComponentSpaceTransform(boneToModify);
        FVector BonePos = BoneTrans.GetLocation();
        FVector ParentBonePos = FVector::ZeroVector;
        if (!boneToModify.IsRootBone()) {
            ParentBonePos = Output.Pose.GetComponentSpaceTransform(Output.Pose.GetPose().GetParentBoneIndex(boneToModify)).GetLocation();
        }
        else {
            ParentBonePos = BonePos - FVector(0, 0, BonePos.Z);
            if (BonePos.Z == 0) {
                ParentBonePos.Z -= 0.1;
            }
        }
        FVector ParentToCurrentUV = (BonePos - ParentBonePos).GetUnsafeNormal();

        FVector* res = nullptr;
        if (!boneToModify.IsRootBone()) {
            res = transformed.Find(Output.Pose.GetPose().GetParentBoneIndex(boneToModify).GetInt());
        }

        FVector newLoc = ParentBonePos + ParentToCurrentUV * measurement.applied;
        if (res != nullptr) {
            newLoc = *res + ParentToCurrentUV * measurement.applied;
        }

        transformed.Add(boneToModify.GetInt(), newLoc);

        BoneTrans.SetLocation(newLoc);

        OutBoneTransforms.Add(FBoneTransform(boneToModify, BoneTrans));
    }


    OutBoneTransforms.Sort(FCompareBoneTransformIndex());
}

bool FCalibrationNode::IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) {
    for (auto& bone : BonesToModify) {
        if (!bone.IsValidToEvaluate(RequiredBones)) return false;
    }
    return true;
}

void FCalibrationNode::GatherDebugData(FNodeDebugData& DebugData) {
    FString DebugLine = DebugData.GetNodeName(this);

    DebugData.AddDebugItem(DebugLine);

    ComponentPose.GatherDebugData(DebugData);
}