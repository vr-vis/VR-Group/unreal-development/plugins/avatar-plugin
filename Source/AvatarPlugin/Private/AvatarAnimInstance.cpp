// Fill out your copyright notice in the Description page of Project Settings.


#include "AvatarAnimInstance.h"

void UAvatarAnimInstance::NativeInitializeAnimation() {
	Super::NativeInitializeAnimation();
}

void UAvatarAnimInstance::NativeUpdateAnimation(float DeltaSeconds) {
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (!CalibrationComponent) {
		APawn* Pawn = TryGetPawnOwner();
		if (Pawn) {
			CalibrationComponent = Cast<UAvatarComponent>(Pawn->GetComponentByClass(UAvatarComponent::StaticClass()));
		}
	}

}
