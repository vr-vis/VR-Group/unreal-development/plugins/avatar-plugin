// Fill out your copyright notice in the Description page of Project Settings.


#include "Measurements.h"
#include "AvatarComponent.h"

UMeasurements::UMeasurements() {
	activeMeasurement = 0;
	calcedAppliedLengths = false;
}

void UMeasurements::Init(const FString& JsonInstructionsFilePath) {
	defaultMeasurementSteps.Empty();
	defaultMeasurementSteps.Append({ 
		MeasurementName::WELCOME,
		MeasurementName::HEIGHT,
		MeasurementName::OFFSETS,
		MeasurementName::DONE
	});
	ConfigureNewInstructions(JsonInstructionsFilePath, defaultMeasurementSteps, true, true, false, true);
	activeMeasurement = 0;
	calcedAppliedLengths = false;
}

void UMeasurements::TakeMeasurement(const FVRPose& pose, const FVRPose& pose_no_pos_offsets, const USkeletalMeshComponent* Mesh, float FloorHeight) {

	calcedAppliedLengths = false;
	float FootHeight;
	
	if (activeMeasurement < measurements.Num()) {

		FMeasurement& meas = measurements[activeMeasurement];

		switch (meas.enumName) {

		case MeasurementName::ELBOW_WRIST:
		case MeasurementName::SHOULDER_WRIST:
		case MeasurementName::SHOULDER_SHOULDER:
			meas.measuredLength = (pose.RightHandPos - pose.LeftHandPos).Size();
			break;

		case MeasurementName::HEIGHT:
		
			break;

		case MeasurementName::KNEE:
			FootHeight = FloorHeight + ((pose.LeftHandPos.Z - FloorHeight) / (Mesh->GetSocketLocation(BoneNames.calf_l).Z - FloorHeight)) * (Mesh->GetSocketLocation(BoneNames.foot_l).Z - FloorHeight);
			meas.measuredLength = pose.LeftHandPos.Z - FootHeight;
			break;

		case MeasurementName::HIP:
			FootHeight = FloorHeight + ((pose.LeftHandPos.Z - FloorHeight) / (Mesh->GetSocketLocation(BoneNames.thigh_l).Z - FloorHeight)) * (Mesh->GetSocketLocation(BoneNames.foot_l).Z - FloorHeight);
			meas.measuredLength = (pose.RightHandPos - pose.LeftHandPos).Size();
			meas.measuredAdditionalHeight = pose.LeftHandPos.Z - FootHeight;
			meas.measuredAdditionalFloorHeight = pose.LeftHandPos.Z - FloorHeight;
			break;

		case MeasurementName::OFFSETS:

			CalcOffsets(pose_no_pos_offsets, Mesh);

			break;

		default:
			break;
		}

	}
}

void UMeasurements::CalcOffsets(const FVRPose& pose, const USkeletalMeshComponent* Mesh) {
	TArray<FString> OffsetPoints = {"Head", "HandL", "HandR", "FootL", "FootR"};
	
	for (FString& Str : OffsetPoints) {

		FTransform* Offset = nullptr;
		FTransform CaptureTrans;
		FName BoneName;

		if (Str == "Head") {
			Offset = &Offsets.Head;
			CaptureTrans.SetLocation(pose.HMDPos);
			CaptureTrans.SetRotation(pose.HMDRot.Quaternion());
			BoneName = BoneNames.head;
		}
		else if (Str == "HandL") {
			Offset = &Offsets.HandL;
			CaptureTrans.SetLocation(pose.LeftHandPos);
			CaptureTrans.SetRotation(pose.LeftHandRot.Quaternion());
			BoneName = BoneNames.hand_l;
		}
		else if (Str == "HandR") {
			Offset = &Offsets.HandR;
			CaptureTrans.SetLocation(pose.RightHandPos);
			CaptureTrans.SetRotation(pose.RightHandRot.Quaternion());
			BoneName = BoneNames.hand_r;
		}
		else if (Str == "FootL") {
			Offset = &Offsets.FootL;
			CaptureTrans.SetLocation(pose.LeftFootPos);
			CaptureTrans.SetRotation(pose.LeftFootRot.Quaternion());
			BoneName = BoneNames.foot_l;
		}
		else if (Str == "FootR") {
			Offset = &Offsets.FootR;
			CaptureTrans.SetLocation(pose.RightFootPos);
			CaptureTrans.SetRotation(pose.RightFootRot.Quaternion());
			BoneName = BoneNames.foot_r;
		}

		FTransform SkelTrans(Mesh->GetSocketQuaternion(BoneName), Mesh->GetSocketLocation(BoneName));
		*Offset = SkelTrans.GetRelativeTransform(CaptureTrans);

	}
}

int UMeasurements::NextMeasurementStep() {
	activeMeasurement++;
	return activeMeasurement;
}

void UMeasurements::ConfigureNewInstructions(const FString& JsonInstructionsFilePath, const TArray<MeasurementName>& measurementSteps, bool withAvatar, bool useFootControllers, bool oculus, bool onlyScale) {
	
	measurements.Empty();

	for (auto step : measurementSteps) {
		FMeasurement meas;
		meas.enumName = step;
		measurements.Emplace(meas);
	}

	ConfigureInstructions(JsonInstructionsFilePath, withAvatar, useFootControllers, oculus, onlyScale);
}

void UMeasurements::ConfigureInstructions(const FString& JsonInstructionsFilePath, bool withAvatar, bool useFootControllers, bool oculus, bool onlyScale) {

	if (LoadTextsFromJson(JsonInstructionsFilePath)) {

		for (FMeasurement& meas : measurements) {
			FString typeString = StaticEnum<MeasurementName>()->GetValueAsString(meas.enumName);
			typeString.Split("::", nullptr, &typeString);

			meas.name = FName(TextsJson->GetStringField(typeString + "_name"));
			if ((meas.enumName == MeasurementName::START || meas.enumName == MeasurementName::OFFSETS) && useFootControllers) {
				meas.instructions = TextsJson->GetStringField(typeString + "_instr_foot");
			}
			else if (meas.enumName == MeasurementName::WELCOME && withAvatar) {
				meas.instructions = TextsJson->GetStringField(typeString + "_instr_avatar");
			}
			else if (meas.enumName == MeasurementName::WELCOME && onlyScale) {
				meas.instructions = TextsJson->GetStringField(typeString + "_only_scale");
			}
			else if (meas.enumName == MeasurementName::MEASLOCINFO && oculus) {
				meas.instructions = TextsJson->GetStringField(typeString + "_instr_oculus");
				meas.imgPath = TextsJson->GetStringField(typeString + "_img_oculus");
			}
			else {
				meas.instructions = TextsJson->GetStringField(typeString + "_instr");
			}
			if (meas.enumName == MeasurementName::MEASLOCINFO && !oculus) {
				meas.imgPath = TextsJson->GetStringField(typeString + "_img");
			}
		}

	}

}

void UMeasurements::AddAppliedLength(const FName& endBone, float appliedLength) {
	appliedLengths.Emplace(FAppliedLength(endBone, std::abs(appliedLength)));
}

void UMeasurements::CalcAppliedLengths(USkeletalMeshComponent* skeletalMesh) {
	
	if (lockCalcAppliedLengths || skeletalMesh == nullptr) {
		return;
	}

	float elbow_wrist = 1;
	float shoulder_wrist = 1;
	float shoulder_shoulder = 1;
	float hip_width = 1;
	float hip_height = 1;
	float hip_floor_height = 1;
	float knee_height = 1;

	for (auto& measurement : measurements) {
		switch (measurement.enumName) {
		case MeasurementName::ELBOW_WRIST:
			elbow_wrist = measurement.measuredLength;
			break;
		case MeasurementName::SHOULDER_WRIST:
			shoulder_wrist = measurement.measuredLength;
			break;
		case MeasurementName::SHOULDER_SHOULDER:
			shoulder_shoulder = measurement.measuredLength;
			break;
		case MeasurementName::HIP:
			hip_width = measurement.measuredLength;
			hip_height = measurement.measuredAdditionalHeight;
			hip_floor_height = measurement.measuredAdditionalFloorHeight;
			break;
		case MeasurementName::KNEE:
			knee_height = measurement.measuredLength;
			break;
		default:
			break;
		}
	}

	const FBoneNames& names = BoneNames;

	appliedLengths.Empty();

	AddAppliedLength(names.hand_l, elbow_wrist);
	AddAppliedLength(names.hand_r, elbow_wrist);

	AddAppliedLength(names.lowerarm_l, shoulder_wrist - elbow_wrist - 3.f);
	AddAppliedLength(names.lowerarm_r, shoulder_wrist - elbow_wrist - 3.f);

	AddAppliedLength(names.upperarm_l, 2.f * shoulder_shoulder / 5.f);
	AddAppliedLength(names.upperarm_r, 2.f * shoulder_shoulder / 5.f);

	AddAppliedLength(names.pelvis, hip_floor_height);

	AddAppliedLength(names.foot_l, knee_height);
	AddAppliedLength(names.foot_r, knee_height);

	AddAppliedLength(names.calf_l, hip_height - knee_height);
	AddAppliedLength(names.calf_r, hip_height - knee_height);

	AddAppliedLength(names.thigh_l, hip_width / 2.5f);
	AddAppliedLength(names.thigh_r, hip_width / 2.5f);

	calcedAppliedLengths = true;
}

int32 UMeasurements::Num() {
	return measurements.Num();
}

void UMeasurements::ResetMeasurements() {
	activeMeasurement = 0;
	appliedLengths.Empty();
	calcedAppliedLengths = false;
	appliedScale = 1.f;
}

TArray<FMeasurement> UMeasurements::GetMeasurementsArray() {
	return measurements;
}

void UMeasurements::SetMeasurementsArray(const TArray<FMeasurement>& measurementArray) {
	measurements = measurementArray;
	calcedAppliedLengths = false;
	activeMeasurement = 0;
}

void UMeasurements::SaveToFile(const FString& filePath, bool HandsSwitched, bool FeetSwitched) {

	FString string;

	for (int i = 0; i < measurements.Num(); i++) {

			//get string of MeasurementName
			FString typeString = StaticEnum<MeasurementName>()->GetValueAsString(measurements[i].enumName);
			typeString.Split("::", nullptr, &typeString);

			string += FString::FromInt((int)measurements[i].enumName) +
				", " + typeString;

			if (measurements[i].measuredLength > 0) {
				string += ", " + FString::SanitizeFloat(measurements[i].measuredLength);
			}

			if (measurements[i].measuredAdditionalHeight > 0) {
				string += ", " + FString::SanitizeFloat(measurements[i].measuredAdditionalHeight);
			}

			if (measurements[i].measuredAdditionalFloorHeight > 0) {
				string += ", " + FString::SanitizeFloat(measurements[i].measuredAdditionalFloorHeight);
			}

			if (measurements[i].enumName == MeasurementName::OFFSETS) {
				string += ", HeadPos, " + FString::SanitizeFloat(Offsets.Head.GetLocation().X) + ", " + FString::SanitizeFloat(Offsets.Head.GetLocation().Y) + ", " + FString::SanitizeFloat(Offsets.Head.GetLocation().Z);
				string += ", HeadRot, " + FString::SanitizeFloat(Offsets.Head.GetRotation().X) + ", " + FString::SanitizeFloat(Offsets.Head.GetRotation().Y) + ", " + FString::SanitizeFloat(Offsets.Head.GetRotation().Z) + ", " + FString::SanitizeFloat(Offsets.Head.GetRotation().W);
				string += ", HandLPos, " + FString::SanitizeFloat(Offsets.HandL.GetLocation().X) + ", " + FString::SanitizeFloat(Offsets.HandL.GetLocation().Y) + ", " + FString::SanitizeFloat(Offsets.HandL.GetLocation().Z);
				string += ", HandLRot, " + FString::SanitizeFloat(Offsets.HandL.GetRotation().X) + ", " + FString::SanitizeFloat(Offsets.HandL.GetRotation().Y) + ", " + FString::SanitizeFloat(Offsets.HandL.GetRotation().Z) + ", " + FString::SanitizeFloat(Offsets.HandL.GetRotation().W);
				string += ", HandRPos, " + FString::SanitizeFloat(Offsets.HandR.GetLocation().X) + ", " + FString::SanitizeFloat(Offsets.HandR.GetLocation().Y) + ", " + FString::SanitizeFloat(Offsets.HandR.GetLocation().Z);
				string += ", HandRRot, " + FString::SanitizeFloat(Offsets.HandR.GetRotation().X) + ", " + FString::SanitizeFloat(Offsets.HandR.GetRotation().Y) + ", " + FString::SanitizeFloat(Offsets.HandR.GetRotation().Z) + ", " + FString::SanitizeFloat(Offsets.HandR.GetRotation().W);
				string += ", FootLPos, " + FString::SanitizeFloat(Offsets.FootL.GetLocation().X) + ", " + FString::SanitizeFloat(Offsets.FootL.GetLocation().Y) + ", " + FString::SanitizeFloat(Offsets.FootL.GetLocation().Z);
				string += ", FootLRot, " + FString::SanitizeFloat(Offsets.FootL.GetRotation().X) + ", " + FString::SanitizeFloat(Offsets.FootL.GetRotation().Y) + ", " + FString::SanitizeFloat(Offsets.FootL.GetRotation().Z) + ", " + FString::SanitizeFloat(Offsets.FootL.GetRotation().W);
				string += ", FootRPos, " + FString::SanitizeFloat(Offsets.FootR.GetLocation().X) + ", " + FString::SanitizeFloat(Offsets.FootR.GetLocation().Y) + ", " + FString::SanitizeFloat(Offsets.FootR.GetLocation().Z);
				string += ", FootRRot, " + FString::SanitizeFloat(Offsets.FootR.GetRotation().X) + ", " + FString::SanitizeFloat(Offsets.FootR.GetRotation().Y) + ", " + FString::SanitizeFloat(Offsets.FootR.GetRotation().Z) + ", " + FString::SanitizeFloat(Offsets.FootR.GetRotation().W);
			}
			
			string += "\n";

	}

	string += "-1, Scale, " + FString::SanitizeFloat(appliedScale) + "\n";
	string += "-2, HandsSwitched, " + FString::FromInt(HandsSwitched ? 1 : 0) + "\n";
	string += "-3, FeetSwitched, " + FString::FromInt(FeetSwitched ? 1 : 0);
	
	FFileHelper::SaveStringToFile(string, *(FPaths::ProjectContentDir() + filePath));
}

bool UMeasurements::LoadFromFile(const FString& filePath, const FString& JsonInstructionsFilePath, bool& HandsSwitched, bool& FeetSwitched) {

	FString string;

	bool res = FFileHelper::LoadFileToString(string, *(FPaths::ProjectContentDir() + filePath));
	if (!res) {
		return false;
	}

	measurements.Empty();
	calcedAppliedLengths = false;
	activeMeasurement = 0;

	TArray<FString> lines;
	string.ParseIntoArrayLines(lines);

	for (int line = 0; line < lines.Num(); line++) {
		TArray<FString> entries;
		lines[line].ParseIntoArray(entries, TEXT(", "));

		FString mName = entries[1];

		if (mName == "Scale") {
			appliedScale = FCString::Atof(*entries[2]);
			if (FMath::IsNaN(appliedScale)) appliedScale = 1.0f;
			continue;
		}
		if (mName == "HandsSwitched") {
			HandsSwitched = FCString::Atoi(*entries[2]) == 0 ? false : true;
			continue;
		}
		if (mName == "FeetSwitched") {
			FeetSwitched = FCString::Atoi(*entries[2]) == 0 ? false : true;
			continue;
		}

		measurements.Push(FMeasurement());
		FMeasurement& measurement = measurements[measurements.Num() - 1];

		measurement.enumName = (MeasurementName)FCString::Atoi(*entries[0]);

		if (mName == "OFFSETS") {

			int i = 2;
			Offsets.Head = FTransform(
				FQuat(FCString::Atof(*entries[i+5]), FCString::Atof(*entries[i+6]), FCString::Atof(*entries[i+7]), FCString::Atof(*entries[i+8])),
				FVector(FCString::Atof(*entries[i+1]), FCString::Atof(*entries[i+2]), FCString::Atof(*entries[i+3]))
			);
			i = 11;
			Offsets.HandL = FTransform(
				FQuat(FCString::Atof(*entries[i + 5]), FCString::Atof(*entries[i + 6]), FCString::Atof(*entries[i + 7]), FCString::Atof(*entries[i + 8])),
				FVector(FCString::Atof(*entries[i + 1]), FCString::Atof(*entries[i + 2]), FCString::Atof(*entries[i + 3]))
			);
			i = 20;
			Offsets.HandR = FTransform(
				FQuat(FCString::Atof(*entries[i + 5]), FCString::Atof(*entries[i + 6]), FCString::Atof(*entries[i + 7]), FCString::Atof(*entries[i + 8])),
				FVector(FCString::Atof(*entries[i + 1]), FCString::Atof(*entries[i + 2]), FCString::Atof(*entries[i + 3]))
			);
			i = 29;
			Offsets.FootL = FTransform(
				FQuat(FCString::Atof(*entries[i + 5]), FCString::Atof(*entries[i + 6]), FCString::Atof(*entries[i + 7]), FCString::Atof(*entries[i + 8])),
				FVector(FCString::Atof(*entries[i + 1]), FCString::Atof(*entries[i + 2]), FCString::Atof(*entries[i + 3]))
			);
			i = 38;
			Offsets.FootR = FTransform(
				FQuat(FCString::Atof(*entries[i + 5]), FCString::Atof(*entries[i + 6]), FCString::Atof(*entries[i + 7]), FCString::Atof(*entries[i + 8])),
				FVector(FCString::Atof(*entries[i + 1]), FCString::Atof(*entries[i + 2]), FCString::Atof(*entries[i + 3]))
			);


		}
		else {
			if (entries.Num() >= 3) {
				measurement.measuredLength = FCString::Atof(*entries[2]);
			}

			if (entries.Num() >= 4) {
				measurement.measuredAdditionalHeight = FCString::Atof(*entries[3]);
			}

			if (entries.Num() >= 5) {
				measurement.measuredAdditionalFloorHeight = FCString::Atof(*entries[4]);
			}
		}
		
	}

	ConfigureInstructions(JsonInstructionsFilePath, true, true, false, true);

	return true;
}

bool UMeasurements::LoadFromFileAndConfigureInstructions(const FString& filePath, const FString& JsonInstructionsFilePath, bool withAvatar, bool useFootControllers, bool oculus, bool onlyScale, bool& HandsSwitched, bool& FeetSwitched) {
	bool res = LoadFromFile(filePath, JsonInstructionsFilePath, HandsSwitched, FeetSwitched);
	if (!res) {
		return res;
	}
	ConfigureInstructions(JsonInstructionsFilePath, withAvatar, useFootControllers, oculus, onlyScale);
	return res;
}

bool UMeasurements::LoadTextsFromJson(const FString& JsonInstructionsFilePath)
{
	if (JsonLoaded) {
		return true;
	}
	FString JsonString;
	FString path = FPaths::ProjectDir() + JsonInstructionsFilePath;
	if (!FFileHelper::LoadFileToString(JsonString, *path))
	{
		JsonLoaded = false;
		return false;
	}
	TextsJson = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<TCHAR>> Reader = FJsonStringReader::Create(JsonString);
	FJsonSerializer::Deserialize(Reader, TextsJson);

	JsonLoaded = true;
	return true;
}

FAppliedLength::FAppliedLength() 
{}

FAppliedLength::FAppliedLength(const FName& endBone, float applied)
	:endBone(endBone), applied(applied)
{}

FBoneNames::FBoneNames() {
	boneroot = TEXT("root");
	spine01 = TEXT("spine_01");
	spine02 = TEXT("spine_02");
	spine03 = TEXT("spine_03");
	spine04 = TEXT("");
	spine05 = TEXT("");

	neck = TEXT("neck_01");
	neck02 = TEXT("");
	head = TEXT("head");
	eye_r = TEXT("cc_base_r_eye");
	eye_l = TEXT("cc_base_l_eye");
	jaw = TEXT("cc_base_jawroot");
	teeth_lower = TEXT("cc_base_jawroot");
	mouth_lower = TEXT("cc_base_jawroot");

	clavicle_l = TEXT("clavicle_l");
	clavicle_r = TEXT("clavicle_r");
	upperarm_l = TEXT("upperarm_l");
	upperarm_r = TEXT("upperarm_r");
	lowerarm_l = TEXT("lowerarm_l");
	lowerarm_r = TEXT("lowerarm_r");
	hand_l = TEXT("hand_l");
	hand_r = TEXT("hand_r");

	pelvis = TEXT("pelvis");
	calf_l = TEXT("calf_l");
	calf_r = TEXT("calf_r");
	thigh_l = TEXT("thigh_l");
	thigh_r = TEXT("thigh_r");
	foot_r = TEXT("foot_r");
	foot_l = TEXT("foot_l");
	foot_socket_r = TEXT("foot_rSocket");
	foot_socket_l = TEXT("foot_lSocket");

}
