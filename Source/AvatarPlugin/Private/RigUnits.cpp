// Fill out your copyright notice in the Description page of Project Settings.


#include "RigUnits.h"

FRigUnit_TwistAngle::FRigUnit_TwistAngle() {
	Axis = FVector(1.f, 0.f, 0.f);
	Quat = FQuat::Identity;
	Result = 0.f;
}

FRigUnit_TwistAngle_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()
	FVector NormAxis = Axis.GetSafeNormal();
	FQuat NormQuat = Quat.GetNormalized();
	Result = NormQuat.GetTwistAngle(NormAxis);
}

FRigUnit_FeetCircularMean::FRigUnit_FeetCircularMean() {
	RotLeft = FQuat::Identity;
	RotRight = FQuat::Identity;
	Result = 0.f;
	SwitchMode = false;
	PrevLeftDeg = 0.f;
	PrevRightDeg = 0.f;
}

FRigUnit_FeetCircularMean_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

	FQuat LeftRot = RotLeft.GetNormalized();
	FQuat RightRot = RotRight.GetNormalized();

	float leftangleRad = LeftRot.GetTwistAngle(LeftRot.GetAxisZ());
	float rightangleRad = RightRot.GetTwistAngle(RightRot.GetAxisZ());
	float leftangleDeg = FMath::UnwindDegrees(FMath::RadiansToDegrees(leftangleRad) - LeftRotOffset);
	float rightangleDeg = FMath::UnwindDegrees(FMath::RadiansToDegrees(rightangleRad) - RightRotOffset);

	LeftDegIn = leftangleDeg;
	RightDegIn = rightangleDeg;

	if ((PrevLeftDeg > 90.f && leftangleDeg < -90) || (PrevLeftDeg < -90.f && leftangleDeg > 90)) {
		SwitchMode = !SwitchMode;
	}
	if ((PrevRightDeg > 90.f && rightangleDeg < -90) || (PrevRightDeg < -90.f && rightangleDeg > 90)) {
		SwitchMode = !SwitchMode;
	}
	
	Result = 0.5f * (rightangleDeg + leftangleDeg);

	if (SwitchMode) {
		Result = FMath::UnwindDegrees(Result + 180.f);
	}

	PrevLeftDeg = leftangleDeg;
	PrevRightDeg = rightangleDeg;

	OutDeg = Result;

	Result = FMath::DegreesToRadians(Result);

}

FRigUnit_InterpolateStep::FRigUnit_InterpolateStep() {

	Time = 0.f;
	IsProcessing = false;
	RightFootActive = true;
	MaxDist = 210.f;
	LeftFootStartPos = FVector::ZeroVector;
	LeftFootStartRot = FQuat::Identity;
	RightFootStartPos = FVector::ZeroVector;
	RightFootStartRot = FQuat::Identity;
	LeftFootTargetPosPrev = FVector::ZeroVector;
	LeftFootTargetRotPrev = FQuat::Identity;
	RightFootTargetPosPrev = FVector::ZeroVector;
	RightFootTargetRotPrev = FQuat::Identity;
	StepDurationPrev = 0.5f;
	StepHeightPrev = 6.f;
	XHeightStretchFactor = 2.f * FMath::Sqrt(StepHeightPrev) / StepDurationPrev;

	StepDuration = StepDurationPrev;
	StepHeight = StepHeightPrev;
	DeltaTime = 0.f;
	LeftFootCurPos = FVector::ZeroVector;
	LeftFootCurRot = FQuat::Identity;
	RightFootCurPos = FVector::ZeroVector;
	RightFootCurRot = FQuat::Identity;
	LeftFootTargetPos = FVector::ZeroVector;
	LeftFootTargetRot = FQuat::Identity;
	RightFootTargetPos = FVector::ZeroVector;
	RightFootTargetRot = FQuat::Identity;
	
	LeftFootPos = FVector::ZeroVector;
	LeftFootRot = FQuat::Identity;
	RightFootPos = FVector::ZeroVector;
	RightFootRot = FQuat::Identity;
}

FRigUnit_InterpolateStep_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

	MaxDist = 200.f;

	if (StepDurationPrev != StepDuration || StepHeightPrev != StepHeight) {
		StepDurationPrev = StepDuration;
		StepHeightPrev = StepHeight;
		XHeightStretchFactor = 2.f * FMath::Sqrt(StepHeightPrev) / StepDurationPrev;
	}

	bool leftChanged = !LeftFootTargetPos.Equals(LeftFootTargetPosPrev) || !LeftFootTargetRot.Equals(LeftFootTargetRotPrev);
	bool rightChanged = !RightFootTargetPos.Equals(RightFootTargetPosPrev) || !RightFootTargetRot.Equals(RightFootTargetRotPrev);

	if (!IsProcessing && (leftChanged || rightChanged)) {

		Time = 0.f;
		IsProcessing = true;

		if (rightChanged) {
			RightFootActive = true;
			RightFootStartPos = RightFootCurPos;
			RightFootStartRot = RightFootCurRot;
			RightFootTargetPosPrev = RightFootTargetPos;
			RightFootTargetRotPrev = RightFootTargetRot;
			LeftFootPos = LeftFootCurPos;
			LeftFootRot = LeftFootCurRot;
			LeftFootPos.Z = LeftFootTargetPos.Z;
		}
		else {
			RightFootActive = false;
			LeftFootStartPos = LeftFootCurPos;
			LeftFootStartRot = LeftFootCurRot;
			LeftFootTargetPosPrev = LeftFootTargetPos;
			LeftFootTargetRotPrev = LeftFootTargetRot;
			RightFootPos = RightFootCurPos;
			RightFootRot = RightFootCurRot;
		}

	}

	if (IsProcessing) {

		if (FVector::Dist(RightFootTargetPos, RightFootCurPos) > MaxDist
			|| FVector::Dist(LeftFootTargetPos, LeftFootCurPos) > MaxDist) {
			RightFootPos = RightFootTargetPos;
			RightFootRot = RightFootTargetRot;
			LeftFootPos = LeftFootTargetPos;
			LeftFootRot = LeftFootTargetRot;
			IsProcessing = false;
			return;
		}

		Time += DeltaTime;
		float alpha = Time / StepDuration;
		if (alpha > 1.f) {
			alpha = 1.f;
			Time = StepDuration;
		}

		if (RightFootActive) {

			RightFootPos.X = RightFootTargetPosPrev.X * alpha + RightFootStartPos.X * (1.f - alpha);
			RightFootPos.Y = RightFootTargetPosPrev.Y * alpha + RightFootStartPos.Y * (1.f - alpha);

			float height = -(FMath::Square(XHeightStretchFactor * (Time - StepDuration / 2.f)) - StepHeight);
			RightFootPos.Z = RightFootTargetPosPrev.Z + height;

			RightFootRot = FQuat::Slerp(RightFootStartRot, RightFootTargetRotPrev, alpha);

			if (FMath::IsNearlyEqual(alpha, 1.f)) {
				RightFootActive = !RightFootActive;
				RightFootPos.Z = RightFootTargetPosPrev.Z;
				if (LeftFootPos.Equals(LeftFootTargetPos, 0.5f) && LeftFootRot.Equals(LeftFootTargetRot, 0.5f)) {
					IsProcessing = false;
				}
				else {
					Time = 0.f;
					LeftFootStartPos = LeftFootCurPos;
					LeftFootStartRot = LeftFootCurRot;
					LeftFootTargetPosPrev = LeftFootTargetPos;
					LeftFootTargetRotPrev = LeftFootTargetRot;
				}
			}

		}

		else {

			LeftFootPos.X = LeftFootTargetPosPrev.X * alpha + LeftFootStartPos.X * (1.f - alpha);
			LeftFootPos.Y = LeftFootTargetPosPrev.Y * alpha + LeftFootStartPos.Y * (1.f - alpha);

			float height = -(FMath::Square(XHeightStretchFactor * (Time - StepDuration / 2.f)) - StepHeight);
			LeftFootPos.Z = LeftFootTargetPosPrev.Z + height;

			LeftFootRot = FQuat::Slerp(LeftFootStartRot, LeftFootTargetRotPrev, alpha);

			if (FMath::IsNearlyEqual(alpha, 1.f)) {
				RightFootActive = !RightFootActive;
				LeftFootPos.Z = LeftFootTargetPosPrev.Z;
				if (RightFootPos.Equals(RightFootTargetPos, 0.5f) && RightFootRot.Equals(RightFootTargetRot, 0.5f)) {
					IsProcessing = false;
				}
				else {
					Time = 0.f;
					RightFootStartPos = RightFootCurPos;
					RightFootStartRot = RightFootCurRot;
					RightFootTargetPosPrev = RightFootTargetPos;
					RightFootTargetRotPrev = RightFootTargetRot;
				}
			}

		}

	}

}

FRigUnit_AvatarSensorOffsets::FRigUnit_AvatarSensorOffsets() {
	ControlTrans = FTransform::Identity;
	SensorOffset = FTransform::Identity;
	OutTrans = FTransform::Identity;
}

FRigUnit_AvatarSensorOffsets_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

		//NewControl = Bone * Control(-1) * NewControl
		OutTrans = SensorOffset * ControlTrans;

}
