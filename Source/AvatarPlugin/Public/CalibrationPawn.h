// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "AvatarComponent.h"
#include "MotionControllerComponent.h"
#include "XRDeviceVisualizationComponent.h"
#include "Camera/CameraComponent.h"
#include "Haptics/HapticFeedbackEffect_Curve.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMesh.h"
#include "Pawn/RWTHVRPawn.h"
#include "AvatarAnimInstance.h"

#include "CalibrationPawn.generated.h"


UCLASS()
class AVATARPLUGIN_API ACalibrationPawn : public ARWTHVRPawn
{
	GENERATED_BODY()

public:

	USceneComponent* VROrigin;

	UPROPERTY(BlueprintReadOnly)
	UMotionControllerComponent* MC_LeftFoot;
	UPROPERTY(BlueprintReadOnly)
	UMotionControllerComponent* MC_RightFoot;

	UPROPERTY(BlueprintReadOnly)
	USkeletalMeshComponent* Mesh;
	UPROPERTY(BlueprintReadOnly)
	UCapsuleComponent* Capsule;

	UPROPERTY(BlueprintReadOnly)
	UXRDeviceVisualizationComponent* LeftControllerVis;
	UPROPERTY(BlueprintReadOnly)
	UXRDeviceVisualizationComponent* RightControllerVis;
	UPROPERTY(BlueprintReadOnly)
	UXRDeviceVisualizationComponent* LeftFootControllerVis;
	UPROPERTY(BlueprintReadOnly)
	UXRDeviceVisualizationComponent* RightFootControllerVis;

	UStaticMeshComponent* Mesh_LeftHand;
	UStaticMeshComponent* Mesh_RightHand;
	UStaticMeshComponent* Mesh_LeftFoot;
	UStaticMeshComponent* Mesh_RightFoot;

	UHapticFeedbackEffect_Curve* rumbleCurve;

	bool right_trigger_down = false;
	bool left_trigger_down = false;
	bool right_restart_down = false;
	bool left_restart_down = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input|Actions")
	class UInputAction* SubmitRight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input|Actions")
	class UInputAction* SubmitLeft;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input|Actions")
	class UInputAction* RestartRight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input|Actions")
	class UInputAction* RestartLeft;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input")
	class UInputMappingContext* CalibrationInputMapping;

public:

	UPROPERTY(BlueprintReadOnly)
	UAvatarComponent* calibrationComponent;

	ACalibrationPawn(const FObjectInitializer& ObjectInitializer);

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void ShowMeshes(bool show);
	void ShowControllerModels(bool show);
	void SetupSkeletalMesh(USkeletalMesh* mesh);

	void SetCalibrationComponent(UAvatarComponent* component);
	void UpdateVRPose(FVRPose& pose, FVRPose& pose_no_pos_offsets, bool useFootControllers, bool correctVive, bool HandsSwitched, bool FeetSwitched, bool relative = false);
	void UpdateMeshPositions(const FVRPose& pose, bool relative = false);

	void RumbleController(EControllerHand which, bool stop);

	UFUNCTION()
	void RightTriggerPressed();
	UFUNCTION()
	void RightTriggerReleased();
	UFUNCTION()
	void LeftTriggerPressed();
	UFUNCTION()
	void LeftTriggerReleased();

	UFUNCTION()
	void RightRestartPressed();
	UFUNCTION()
	void RightRestartReleased();
	UFUNCTION()
	void LeftRestartPressed();
	UFUNCTION()
	void LeftRestartReleased();

};
