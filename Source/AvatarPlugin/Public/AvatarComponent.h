// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/LevelStreamingDynamic.h"
#include "Sound/SoundWave.h"
#include "Components/AudioComponent.h"
#include "Measurements.h"
#include "CalibrationWidget.h"
#include "Components/WidgetComponent.h"
#include "EngineUtils.h"
#include "Components/SceneComponent.h"
#include "Components/CapsuleComponent.h"
#include "Animation/SkeletalMeshActor.h"
#include "Animation/BlendSpace.h"
#include "AvatarComponent.generated.h"

class ACalibrationPawn;
class UAvatarAnimInstance;

UENUM(BlueprintType)
enum CalibrationMode {
	TEXTONLY = 0 UMETA(DisplayName = "Text Only"),
	AVATARGUIDE = 1 UMETA(DisplayName = "Avatar Guide"),
	SCALE = 2 UMETA(DisplayName = "Scale Only")
};

USTRUCT(BlueprintType)
struct FVRPose {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FVector HMDPos = FVector::ZeroVector;
	UPROPERTY(BlueprintReadOnly)
	FRotator HMDRot = FRotator::ZeroRotator;
	UPROPERTY(BlueprintReadOnly)
	FVector LeftHandPos = FVector::ZeroVector;
	UPROPERTY(BlueprintReadOnly)
	FRotator LeftHandRot = FRotator::ZeroRotator;
	UPROPERTY(BlueprintReadOnly)
	FVector RightHandPos = FVector::ZeroVector;
	UPROPERTY(BlueprintReadOnly)
	FRotator RightHandRot = FRotator::ZeroRotator;
	UPROPERTY(BlueprintReadOnly)
	FVector LeftFootPos = FVector::ZeroVector;
	UPROPERTY(BlueprintReadOnly)
	FRotator LeftFootRot = FRotator::ZeroRotator;
	UPROPERTY(BlueprintReadOnly)
	FVector RightFootPos = FVector::ZeroVector;
	UPROPERTY(BlueprintReadOnly)
	FRotator RightFootRot = FRotator::ZeroRotator;

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AVATARPLUGIN_API UAvatarComponent : public UActorComponent {
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<CalibrationMode> calibrationMode;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString calibrationLevelPath = "/AvatarPlugin/AvatarCalibrationMap";

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FVector calibrationLevelPos;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool useLastCalibrationData;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString CalibrationDataSaveFile = "LastCalibration.txt";

	UPROPERTY(EditAnywhere)
	FString JsonInstructionsFilePath = "Plugins/avatar-plugin/Content/instructions.json";

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USkeletalMeshComponent* SkeletalMeshComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MeshDefaultHeight;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<UAvatarAnimInstance> AnimBPToUse;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool useFootControllers;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool DoFootStepAlgorithm;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* HeadComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* HandLeftComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* HandRightComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* FootLeftComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* FootRightComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool showMeshes;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool showControllers;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FBoneNames BoneNames;

	UPROPERTY(BlueprintReadWrite)
	UMeasurements* measurements;

	UPROPERTY(BlueprintReadWrite)
	bool doIK;

	UPROPERTY(BlueprintReadWrite)
	bool HandsSwitched;

	UPROPERTY(BlueprintReadWrite)
	bool FeetSwitched;

private:

	bool viveMode;
	bool waitForLoad;
	bool stoppedCalibration;
	bool justLoadedWidget;
	bool widgetLoadedCompletely;
	bool isInspecting;
	bool isCalibrating;
	bool finishedCalibration;
	ULevelStreamingDynamic* streamingLevel;
	APawn* oldPossessedPawn;
	ACalibrationPawn* pawn;
	ASkeletalMeshActor* helperMesh;
	AActor* leftFootSphere;
	AActor* rightFootSphere;
	AActor* mirrorPlayerStart;
	UPROPERTY()
	UCalibrationWidget* widget;

	bool waitForRelease;
	volatile bool leftTriggerPressed;
	volatile bool rightTriggerPressed;
	volatile bool shouldPressLeftTrigger;
	volatile bool shouldPressRightTrigger;
	bool prevPressedSuccess;
	float triggerPressStartTime;
	float confirmTime = 1.5f;

	TMap<FName, UAudioComponent*> playingSounds;
	TMap<FName, USoundWave*> soundWaves;
	TMap<FString, UTexture2D*> images;
	FString helperCapturePath;

	UPROPERTY()
	UAnimationAsset* Anim_Welcome;
	UPROPERTY()
	UAnimationAsset* Anim_ElbowWrist;
	UPROPERTY()
	UAnimationAsset* Anim_ShoulderWrist;
	UPROPERTY()
	UAnimationAsset* Anim_ShoulderShoulder;
	UPROPERTY()
	UAnimationAsset* Anim_Hip;
	UPROPERTY()
	UAnimationAsset* Anim_Knee;
	UPROPERTY()
	UAnimationAsset* Anim_Done;

	FVRPose pose;
	FVRPose pose_no_pos_offsets;
	float footControllerHeightOffset;

	UPROPERTY()
	USkeletalMeshComponent* OldSkeletalMeshComponent;
	UPROPERTY()
	USceneComponent* OldHeadComponent;
	UPROPERTY()
	USceneComponent* OldHandLeftComponent;
	UPROPERTY()
	USceneComponent* OldHandRightComponent;
	UPROPERTY()
	USceneComponent* OldFootLeftComponent;
	UPROPERTY()
	USceneComponent* OldFootRightComponent;

public:		

	UAvatarComponent();

protected:

	virtual void BeginPlay() override;
	bool MeasurementsInited = false;
	void InitMeasurements();

public:	

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void StartCalibration();

	void StopCalibration();

	UFUNCTION(BlueprintCallable)
	bool IsCalibrating();
	UFUNCTION(BlueprintPure)
	bool IsInspecting();
	UFUNCTION(BlueprintCallable)
	bool FinishedCalibration();
	UFUNCTION(BlueprintCallable)
	void FinishCalibration();

	float GetConfirmTimePercentage();
	UFUNCTION(BlueprintCallable)
	UMeasurements* GetMeasurements();
	UFUNCTION(BlueprintCallable)
	bool CalcAppliedLengths();

	UFUNCTION(BlueprintCallable)
	void SetBoneNames(const FBoneNames& boneNames_);

	virtual bool CanEditChange(const FProperty* InProperty) const override;

private:

	void StartInspection();
	void UpdateCalibration();
	void UpdateInspection();
	void NextMeasurementStep();
	void TryLoadWidget();
	void SetWidgetInformation(int measurementNr);

	void OnLevelLoaded();
	void OnStopCalibration();

	void PlaySound(const FName& name, float vol = 1.f, bool threeDimensional = false, const FVector& loc = FVector::ZeroVector);
	void StopSound(const FName& name);

	void SwitchHands();
	void SwitchFeet();

};
