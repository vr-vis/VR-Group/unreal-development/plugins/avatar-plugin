// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "KismetAnimationLibrary.h"
#include "Dom/JsonObject.h"
#include "Measurements.generated.h"

struct FVRPose;

USTRUCT(BlueprintType)
struct FOffsets {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
		FTransform Head;
	UPROPERTY(BlueprintReadOnly)
		FTransform HandL;
	UPROPERTY(BlueprintReadOnly)
		FTransform HandR;
	UPROPERTY(BlueprintReadOnly)
		FTransform FootL;
	UPROPERTY(BlueprintReadOnly)
		FTransform FootR;
};

USTRUCT(BlueprintType)
struct FBoneNames {

	GENERATED_BODY() 

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName boneroot;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName spine01;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName spine02;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName spine03;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName spine04;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName spine05;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName neck;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName neck02;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName head;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName eye_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName eye_l;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName jaw;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName teeth_lower;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName mouth_lower;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName clavicle_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName clavicle_l;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName upperarm_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName upperarm_l;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName lowerarm_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName lowerarm_l;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName hand_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName hand_l;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName pelvis;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName calf_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName calf_l;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName thigh_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName thigh_l;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName foot_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName foot_l;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName foot_socket_r;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName foot_socket_l;

	FBoneNames();
};

UENUM()
enum class MeasurementName {
	START,
	WELCOME,
	MEASLOCINFO,
	HEIGHT,
	ELBOW_WRIST,
	SHOULDER_WRIST,
	SHOULDER_SHOULDER,
	HIP,
	KNEE,
	OFFSETS,
	DONE
};

USTRUCT(BlueprintType)
struct FMeasurement {
	GENERATED_BODY()

	MeasurementName enumName;
	FName name = "";
	FString instructions = "";
	FString imgPath = "";
	bool leftShouldBePressed = true;
	bool rightShouldBePressed = true;
	float measuredLength = -1;
	float measuredAdditionalHeight = -1;
	float measuredAdditionalFloorHeight = -1;
};

USTRUCT()
struct FAppliedLength {
	GENERATED_BODY()

	FName endBone;
	float applied = 1;

	FAppliedLength();
	FAppliedLength(const FName& endBone, float applied);
};

UCLASS(Blueprintable)
class AVATARPLUGIN_API UMeasurements : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly)
	bool lockCalcAppliedLengths = false;
	int activeMeasurement;
	bool calcedAppliedLengths = false;
	TArray<FAppliedLength> appliedLengths;
	UPROPERTY(BlueprintReadWrite)
	float appliedScale;
	TArray<FMeasurement> measurements;
	TArray<MeasurementName> defaultMeasurementSteps;

	UPROPERTY(BlueprintReadWrite)
	FOffsets Offsets;

	FBoneNames BoneNames;

	UMeasurements();
	void Init(const FString& JsonInstructionsFilePath);

	void TakeMeasurement(const FVRPose& pose, const FVRPose& pose_no_pos_offsets, const USkeletalMeshComponent* Mesh, float FloorHeight);
	void CalcOffsets(const FVRPose& pose_no_pos_offsets, const USkeletalMeshComponent* Mesh);
	int NextMeasurementStep();
	UFUNCTION(BlueprintCallable)
	void ConfigureNewInstructions(const FString& JsonInstructionsFilePath, const TArray<MeasurementName>& measurementSteps, bool withAvatar, bool useFootControllers, bool oculus, bool onlyScale);
	UFUNCTION(BlueprintCallable)
	void ConfigureInstructions(const FString& JsonInstructionsFilePath, bool withAvatar, bool useFootControllers, bool oculus, bool onlyScale);
	void AddAppliedLength(const FName& endBone, float appliedLength);
	UFUNCTION(BlueprintCallable)
	void CalcAppliedLengths(USkeletalMeshComponent* skeletalMesh);
	int32 Num();
	void ResetMeasurements();
	
	UFUNCTION(BlueprintCallable)
	TArray<FMeasurement> GetMeasurementsArray();
	UFUNCTION(BlueprintCallable)
	void SetMeasurementsArray(const TArray<FMeasurement>& measurementArray);

	UFUNCTION(BlueprintCallable)
	void SaveToFile(const FString& filePath, bool HandsSwitched, bool FeetSwitched);
	
	//Loads a not reduced file without configuring instructions (call ConfigureInstructions(bool, bool, bool) after to get the instruction strings)
	UFUNCTION(BlueprintCallable)
	bool LoadFromFile(const FString& filePath, const FString& JsonInstructionsFilePath, bool& HandsSwitched, bool& FeetSwitched);
	//Loads a not reduced file with configuring instructions
	UFUNCTION(BlueprintCallable)
	bool LoadFromFileAndConfigureInstructions(const FString& filePath, const FString& JsonInstructionsFilePath, bool withAvatar, bool useFootControllers, bool oculus, bool onlyScale, bool& HandsSwitched, bool& FeetSwitched);

	TSharedPtr<FJsonObject> TextsJson;

	bool LoadTextsFromJson(const FString& JsonInstructionsFilePath);

private:

	bool JsonLoaded = false;

};