// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Blueprint/WidgetTree.h"
#include "Components/CanvasPanel.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Kismet/GameplayStatics.h"

#include "CalibrationWidget.generated.h"

class UAvatarCalibration;

UCLASS()
class AVATARPLUGIN_API UCalibrationWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite)
	UCanvasPanel* canvas;
	UPROPERTY(BlueprintReadWrite)
	UImage* bg_image;
	UPROPERTY(BlueprintReadWrite)
	UImage* help_image;
	UPROPERTY(BlueprintReadWrite)
	UProgressBar* progbar;
	UPROPERTY(BlueprintReadWrite)
	UTextBlock* text_name;
	UPROPERTY(BlueprintReadWrite)
	UTextBlock* text_instructions;
	UPROPERTY(BlueprintReadWrite)
	UTextBlock* text_continue;

	UCalibrationWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

};
