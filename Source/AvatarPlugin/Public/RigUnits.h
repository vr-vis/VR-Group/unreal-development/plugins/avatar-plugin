// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Units/RigUnit.h"
#include "RigUnits.generated.h"

USTRUCT(meta = (DisplayName = "Twist Angle", PrototypeName = "TwistAngle", Category = "Math|Quaternion", MenuDescSuffix = "(Quaternion)", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_TwistAngle : public FRigUnit
{
	GENERATED_BODY()

	FRigUnit_TwistAngle();

	RIGVM_METHOD()
		virtual void Execute() override;

	UPROPERTY(meta = (Input))
	FVector Axis;

	UPROPERTY(meta = (Input))
	FQuat Quat;

	UPROPERTY(meta = (Output))
	float Result;

};

USTRUCT(meta = (DisplayName = "Feet Circular Mean", PrototypeName = "FeetCircularMean", Category = "Math", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_FeetCircularMean : public FRigUnit
{
	GENERATED_BODY()

	FRigUnit_FeetCircularMean();

	RIGVM_METHOD()
		virtual void Execute() override;

	UPROPERTY()
	bool SwitchMode;

	UPROPERTY()
	float PrevLeftDeg;

	UPROPERTY()
	float PrevRightDeg;

	UPROPERTY(meta = (Input))
	FQuat RotLeft;

	UPROPERTY(meta = (Input))
	FQuat RotRight;

	UPROPERTY(meta = (Input))
	float LeftRotOffset = 0.0f;

	UPROPERTY(meta = (Input))
	float RightRotOffset = 0.0f;

	UPROPERTY(meta = (Output))
	float LeftDegIn = 0.0f;

	UPROPERTY(meta = (Output))
	float RightDegIn = 0.0f;

	UPROPERTY(meta = (Output))
	float OutDeg = 0.0f;

	UPROPERTY(meta = (Output))
	float Result = 0.0f;

};

USTRUCT(meta = (DisplayName = "Interpolate Step", PrototypeName = "InterpolateStep", Category = "Math", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_InterpolateStep : public FRigUnit
{
	GENERATED_BODY()

	FRigUnit_InterpolateStep();

	RIGVM_METHOD()
		virtual void Execute() override;

	UPROPERTY()
	float Time;

	UPROPERTY()
	bool IsProcessing;

	UPROPERTY()
	bool RightFootActive;

	UPROPERTY()
	float XHeightStretchFactor;

	UPROPERTY()
	float MaxDist;

	UPROPERTY()
	FVector LeftFootStartPos;

	UPROPERTY()
	FQuat LeftFootStartRot;

	UPROPERTY()
	FVector RightFootStartPos;

	UPROPERTY()
	FQuat RightFootStartRot;

	UPROPERTY()
	FVector LeftFootTargetPosPrev;

	UPROPERTY()
	FQuat LeftFootTargetRotPrev;

	UPROPERTY()
	FVector RightFootTargetPosPrev;

	UPROPERTY()
	FQuat RightFootTargetRotPrev;

	UPROPERTY()
	float StepDurationPrev;

	UPROPERTY()
	float StepHeightPrev;


	UPROPERTY(meta = (Input))
	float StepDuration;

	UPROPERTY(meta = (Input))
	float StepHeight;

	UPROPERTY(meta = (Input))
	float DeltaTime;

	UPROPERTY(meta = (Input))
	FVector LeftFootCurPos;

	UPROPERTY(meta = (Input))
	FQuat LeftFootCurRot;

	UPROPERTY(meta = (Input))
	FVector RightFootCurPos;

	UPROPERTY(meta = (Input))
	FQuat RightFootCurRot;

	UPROPERTY(meta = (Input))
	FVector LeftFootTargetPos;

	UPROPERTY(meta = (Input))
	FQuat LeftFootTargetRot;

	UPROPERTY(meta = (Input))
	FVector RightFootTargetPos;

	UPROPERTY(meta = (Input))
	FQuat RightFootTargetRot;


	UPROPERTY(meta = (Output))
	FVector LeftFootPos;

	UPROPERTY(meta = (Output))
	FQuat LeftFootRot;

	UPROPERTY(meta = (Output))
	FVector RightFootPos;

	UPROPERTY(meta = (Output))
	FQuat RightFootRot;

};

USTRUCT(meta = (DisplayName = "AvatarSensorOffsets", PrototypeName = "AvatarSensorOffsets", Category = "Hierarchy", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_AvatarSensorOffsets : public FRigUnit
{
	GENERATED_BODY()

	FRigUnit_AvatarSensorOffsets();

	RIGVM_METHOD()
		virtual void Execute() override;

	UPROPERTY(meta = (Input))
	FTransform ControlTrans;

	UPROPERTY(meta = (Input))
	FTransform SensorOffset;

	UPROPERTY(meta = (Output))
	FTransform OutTrans;

};