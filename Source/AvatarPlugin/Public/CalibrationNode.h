// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BoneControllers/AnimNode_SkeletalControlBase.h"
#include "Measurements.h"

#include "CalibrationNode.generated.h"

USTRUCT(BlueprintType)
struct AVATARPLUGIN_API FCalibrationNode : public FAnimNode_SkeletalControlBase
{
	GENERATED_BODY()
	
    TArray<FBoneReference> BonesToModify;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
    USkeletalMeshComponent* skeletalMesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
    UMeasurements* measurements;

    virtual void EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) override;
    virtual bool IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) override;
    virtual void GatherDebugData(FNodeDebugData& DebugData) override;

    FCalibrationNode();

private:

    bool SetUpSkeletonBones();

    virtual void InitializeBoneReferences(const FBoneContainer& RequiredBones) override;
};
