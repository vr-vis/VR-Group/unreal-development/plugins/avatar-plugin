using UnrealBuildTool;
using System.IO;

public class AvatarPlugin : ModuleRules
{
    public AvatarPlugin(ReadOnlyTargetRules Target) : base(Target)
    {

        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PrivateIncludePaths.AddRange(new string[] { });
        PublicIncludePaths.AddRange(new string[] { });

        PrivateDependencyModuleNames.AddRange(new string[] { });
        PublicDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine", "Core", "InputCore", "UMG", "XRBase", "HeadMountedDisplay", "AnimGraphRuntime", "ControlRig", "RigVM", "RWTHVRToolkit", "Json", "EnhancedInput" });

    }
}