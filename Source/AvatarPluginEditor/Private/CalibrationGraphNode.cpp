// Fill out your copyright notice in the Description page of Project Settings.


#include "CalibrationGraphNode.h"

#define LOCTEXT_NAMESPACE "AnimGraphNodes"

UCalibrationGraphNode::UCalibrationGraphNode(const FObjectInitializer& ObjectInitializer)
    :Super(ObjectInitializer)
{
}

FLinearColor UCalibrationGraphNode::GetNodeTitleColor() const
{
    return FLinearColor::Green;
}

FText UCalibrationGraphNode::GetTooltipText() const
{
    return LOCTEXT("ApplyCalibration", "ApplyCalibration");
}

FText UCalibrationGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
    return LOCTEXT("ApplyCalibration", "ApplyCalibration");
}

FString UCalibrationGraphNode::GetNodeCategory() const
{
    return TEXT("AvatarCalibration");
}

#undef LOCTEXT_NAMESPACE