using UnrealBuildTool;
using System.IO;

public class AvatarPluginEditor : ModuleRules
{
    public AvatarPluginEditor(ReadOnlyTargetRules Target) : base(Target)
    {

        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PrivateIncludePaths.AddRange(new string[] {  });
        PublicIncludePaths.AddRange(new string[] {  });

        PublicDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine", "Core", "AnimGraph", "AnimGraphRunTime", "BlueprintGraph", "AvatarPlugin" });

    }
}